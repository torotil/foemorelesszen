const presets = [
  [
    '@babel/preset-env',
    {
      targets: {
        ie: '11',
        firefox: '30',
        chrome: '40',
        safari: '7',
      },
      useBuiltIns: 'usage',
      modules: 'commonjs',
    },
  ],
];
const plugins = [];

module.exports = {
  presets,
  plugins,
};
