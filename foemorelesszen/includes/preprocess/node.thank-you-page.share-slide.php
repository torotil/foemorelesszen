<?php
global $base_url;
  // Construct the required parts of the share slide, by pulling metadata from
  // a supplied or generated URL, and parsing "sharelight" values into a
  // custom array for our display.

  // Called within foemorelesszen_preprocess_node, with direct access to
  // the $variables array (reference).

  // Set up defaults
  $share['target'] = urlencode(url('/', array('absolute' => TRUE)));
  $share['image'] = 'https://placehold.it/1200x628';
  $share['title'] = 'Placeholder title, please check setup';
  $share['description'] = 'Placeholder description, please check setup';


  // Gather available webform submission information
  $sid = (isset($_GET['sid']) && is_numeric($_GET['sid'])) ? (int) $_GET['sid'] : 0;
  $hash = isset($_GET['hash']) ? $_GET['hash'] : NULL;
  $webform_nid = (isset($_GET['share']) && preg_match('|^node/\d+$|', $_GET['share'])) ? (int) str_replace('node/', '', $_GET['share']) : NULL;

  // Get the submission data (if we have enough information)
  $submission = _campaignion_webform_tokens_get_submission($sid, $hash);

  // If we've got a submission and a webform node ID we can do token
  // replacement later, so load in the webform node.
  if ($submission && $webform_nid) {
    $webform_node = node_load($webform_nid);
  }

  // Set the URL share/preview target to the originating webform if available
  $url_to_preview = $webform_nid ? $base_url . '/node/' . $webform_nid : '';

  // Override with the parameter from share_light, if provided
  if (isset($node->share_light['und'][0]['options']['link']['path'])
       && $node->share_light['und'][0]['options']['link']['path'] != '') {
    $url_to_preview = $node->share_light['und'][0]['options']['link']['path'];
  }

  // Super override (Optional share_override query parameter)
  if (isset($_GET['share_override'])) {
    if (preg_match('|^node/\d+$|', $_GET['share_override'])) {
      $url_to_preview = $base_url . '/' . $_GET['share_override'];
    } elseif (preg_match('|^https?://.+$|', urldecode($_GET['share_override']))) {
      $url_to_preview = urldecode($_GET['share_override']);
    }
  }

  // Replace any webform tokens in the URL
  if (isset($webform_node) && isset($submission) ) {
      $url_to_preview = webform_replace_tokens(
      $url_to_preview,
      $webform_node,
      $submission
    );
  }

  // Pull "live" metadata from the share/preview target
  if (isset($url_to_preview)) {
    $share['target'] = $url_to_preview;
    // -------- Start pilfered code ------------------------------------
    // ( Adapted from: https://stackoverflow.com/a/7454737/6903773 )
    $shareDoc = new DomDocument();
    @$shareDoc->loadHTML(mb_convert_encoding(file_get_contents($url_to_preview), "HTML-ENTITIES", "UTF-8"));
    $xpath = new DOMXPath($shareDoc);
    $query = '//*/meta';
    $metaElements = $xpath->query($query);
    $metaTags = array();
    foreach ($metaElements as $metaElement) {
      $metaName = $metaElement->getAttribute('name');
      $metaProperty = $metaElement->getAttribute('property');
      $metaContent = $metaElement->getAttribute('content');
      if (!empty($metaProperty)) {
        $metaTags[$metaProperty] = $metaContent;
      } else if (!empty($metaName)) {
        $metaTags[$metaName] = $metaContent;
      }
    }
    // -------- End pilfered code ------------------------------------

    // Dump our metatags into $share. "og:" opengraph is higher priority than
    // twitter, since our preview is a facebook mock-up. (Meaning if both are
    // available, "og:" wins.
    foreach(['title', 'description', 'image', 'url'] as $metaType) {
      foreach(['', 'twitter:', 'og:'] as $metaOrg) {
        if(isset($metaTags[$metaOrg . $metaType])) {
          $share[ $metaType ] = $metaTags[$metaOrg . $metaType];
        }
      }
    }

    if (isset($share['url'])) {
      $share['target'] = urlencode($share['url']);
    }

  }

  // Set up share channel options as required
  $share_light = $node->share_light['und'][0]['options'];
  $share_channels = $share_light['channels'];
  $shares = array();

  if (isset($share_channels['facebook'])) {
    $shares['facebook'] = array(
      'name' => 'Facebook',
      'icon' => 'fab fa-facebook-f',
      'base_url' => 'https://www.facebook.com/sharer/sharer.php?u=',
      'url' => $share['target'],
      'mobile_only' => false,
    );
  }

  if (isset($share_channels['twitter'])) {
    $shares['twitter'] = array(
      'name' => 'Twitter',
      'icon' => 'fab fa-twitter',
      'base_url' => 'https://twitter.com/intent/tweet?text=',
      'url' => rawurlencode($share_channels['twitter']['text']) . '%20' . $share['target'],
      'mobile_only' => false,
    );
  }

  if (isset($share_channels['whatsapp'])) {
    $shares['whatsapp'] = array(
      'name' => 'Whatsapp',
      'icon' => 'fab fa-whatsapp',
      'base_url' => 'whatsapp://send?text=',
      'url' => rawurlencode($share_channels['whatsapp']['text']) . '%20' . $share['target'],
      'mobile_only' => true,
    );
  }

  if (isset($share_channels['fbmsg'])) {
    $shares['fbmsg'] = array(
      'name' => 'Messenger',
      'icon' => 'fab fa-facebook-messenger',
      'base_url' => 'fb-messenger://share/?link=',
      'url' => $share['target'],
      'mobile_only' => true,
    );
  }

  if (isset($share_channels['email']) && isset($share_channels['email']['mailto'])) {
    $shares['email'] = array(
      'name' => 'E-mail',
      'icon' => 'far fa-envelope',
      'base_url' => 'mailto:?subject=' . rawurlencode($share_channels['email']['mailto']['subject']),
      'url' => '&body=' . rawurlencode($share_channels['email']['mailto']['body'] . "\n\n" . $share['target']),
      'mobile_only' => false,
    );
  }

  $variables['share'] = $share;
  $variables['shares'] = $shares;
  $variables['share_channels'] = $share_channels;
