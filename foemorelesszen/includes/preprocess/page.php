<?php

/**
 * @see hook_preprocess_page().
 */
function foemorelesszen_preprocess_page (&$vars) {
  global $base_url;

  $prefill_endpoint = 'https://service-prefill-dev.azurewebsites.net'; // Dev Endpoint
  if ((strpos($base_url, 'foeuk.drupal7.more-onion.at') !== FALSE) || (strpos($base_url, 'act.foe.co.uk') !== FALSE) || (strpos($base_url, 'act.friendsoftheearth.uk') !== FALSE)) {
    // This is the prod endpoint.
    $prefill_endpoint = 'https://service-prefill-prod.azurewebsites.net';
  }

  // Append livereload js if we are in local
  if (strpos($base_url, 'foe-campaignion.docker.amazee.io') !== FALSE) {
    drupal_add_js('http://foe-campaignion.docker.amazee.io:9001/livereload.js', 'external');
  }

  drupal_add_js(array('prefillEndpoint' => $prefill_endpoint), 'setting');

  // Custom Error page for 404 and 403 only
  $header = drupal_get_http_header('status');
  if (stripos($header, '404') !== FALSE || stripos($header, '403') !== FALSE) {
    $vars['theme_hook_suggestions'][] = 'page__error';
  }

}
