// Styles
import './scss/master.scss';

// Legacy JS components
import './js/es5/foe.localstorage';
import './js/es5/morelesszen_starterkit';
import './js/es5/read_more';
import './js/es5/form.input-validator';
import './js/es5/webform-steps';
import './js/es5/select_or_other';
import './js/es5/pca_trigger';
import './js/es5/paypal_button_customize';
import './js/es5/add_required_field_asterisks';
import './js/es5/webform-foe-prefill-api';
import './js/es5/behaviour.campaignion_overlay';
import './js/es5/foe.share_light_click_tracking';
import './js/es5/foe.js-tokens';
import './js/es5/donation/donation.tooltip';
import './js/es5/donation/donation.sticker';
import './js/es5/foe.new-ux-fixed-button';
// import './js/es5/webform-tracking'; // Migrated on web-core
// import './js/es5/foe.scroll-to'; // Migrated on web-core

// Quiz scripts
import './js/quiz/main';
