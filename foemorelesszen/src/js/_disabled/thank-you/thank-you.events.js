/**
* @file thank-you.events.js
* @author James South
* @description Drives the GA events on the thank you page
* @created Mon May 14 2018 13:11:29 GMT+0100 (BST)
* @last-modified Mon May 14 2018 13:11:29 GMT+0100 (BST)
*/
 
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.thankyou_events = {
    attach: function(context, settings) {
      // If we can't implement it using the normal [click-track] meta?
      $('#foe-custom-action-ask a', context).attr('target', '_blank').on('click touch', function() {
        ga('send', 'event', {
          eventCategory: 'interaction',
          eventAction: 'scrolling_thank_you_action'
        });
      });
    }
  };
})(jQuery, Drupal);