/**
 * @file add_required_field_asterisks.js
 * Appends an asterisk to any placeholder text present on a required input field.
 */
(function($) {

  Drupal.behaviors.add_required_field_asterisks = {
    attach: function(context) {
      // Add to text field placeholders
      $('input.required, input[required]', context).each(function() {
        // Don't add the asterisk if it's been manually added, or if "(required)" is found in the placeholder
        // (Both were used as workarounds prior to this programmatic insertion.)
        if (this.placeholder && this.placeholder.slice(-1) !== '*' && !/(required)/i.test(this.placeholder)) {
          this.placeholder = this.placeholder + " *";
        }
      });

      // Add to selects (and styled replicas)
      $('select.required > option:first-of-type,' +
        'select[required] > option:first-of-type,' +
        'select[required] + div.selecter > span,' +
        'select[required] + div.selecter > div.selecter-options > span:first-of-type',
        context).each(function() {
        if (this.innerText === '- Select -') {
          this.innerText = '- Select (required) -';
        }
      });
    }
  };

})(jQuery);
