// We're clobbering the behaviour from the campaigion_overlay module

(function($) {
  Drupal.behaviors.campaignionOverlay = {
    attach: function(context, settings) {
      if (settings.campaignion_overlay
          && settings.campaignion_overlay.overlay_enabled
          && settings.campaignion_overlay.overlay_fired === undefined ) {

        settings.campaignion_overlay.overlay_fired = true;
        // Add jQuery functions for animations
        jQuery.fn.extend({
          campaignionOverlayShow: Drupal.behaviors.campaignionOverlay.show,
          campaignionOverlayClose: Drupal.behaviors.campaignionOverlay.close
        });

        // Lock the body in place while the dialog is open.
        $(document.body, context).addClass('overlay-open');

        var overlay = $(".campaignion-overlay-options", context).first();

        overlay.dialog({
          dialogClass: "campaignion-overlay",
          modal: "true",
          resizable: false,
          draggable: false,
          width: "auto",
          height: "auto",
          focus: null,
          show: "campaignionOverlayShow",
          hide: "campaignionOverlayClose",
          closeOnEscape: true,
        });

        // Add custom 'close' button.
        var closeButton = $('<a href="#" class="closeButton" title="' + Drupal.t("Close") + '">' +
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-100 -100 200 200">' +
              '<path stroke-width="20px" stroke="#aaaaaa" d="M-50 -50L50 50Z M50 -50L-50 50Z"></path>' +
            '</svg>' +
          '</a>');
        closeButton.click(function() {
          overlay.dialog("close");
        });
        overlay.find('.campaignion-overlay-content').prepend(closeButton);

        // Add custom 'close' link.
        var closeLink = $('<a href="#" id="closeLink" class="closeLink" title="' + Drupal.t("Close") + '">No thanks</a>');
        closeLink.click(function() {
          overlay.dialog("close");
        });
        overlay.find('.campaignion-overlay-content').append(closeLink);
        //appending made the text disappear for some reason so readding it
        $("#closeLink").text('No thanks');        

        // generic class to use with e.g. custom buttons
        $('.campaignion-overlay-close', context).click(function(event) {
          overlay.dialog("close");
        });
      }
    }
  };

  /**
   * Provides a fade in type animation.
   *
   * The object is moved up from 5% below its actual `top` property
   * and its opacity is toggled.
   */
  Drupal.behaviors.campaignionOverlay.show = function() {
    var top_ = parseFloat($(this).css('top').replace('px', ''));
    var move = $(window).height()/100 * 5;
    var new_top = (top_+move).toString().concat('px');

    $(this).css('top', new_top);
    $(this).animate({
      top: top_.toString().concat('px'),
      opacity: "toggle"
    }, 750, "swing");

  };

  /**
   * Provides a fade out type animation.
   *
   * The object is moved up to 5% above its actual `top` property
   * and its opacity is toggled.
   */
  Drupal.behaviors.campaignionOverlay.close = function() {
    var top_ = parseFloat($(this).css('top').replace('px', ''));
    var move = $(window).height()/100 * 5;
    var new_top = (top_-move).toString().concat('px');

    $(this).animate({
      top: new_top,
      opacity: "toggle"
    }, 750, "swing", function() {
      $(this).hide();
    });

    // Unlock the body
    $(document.body).removeClass('overlay-open');

  };

})(jQuery);
