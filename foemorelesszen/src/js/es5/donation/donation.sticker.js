/**
* @file donation.sticker.js
* @author Luigi Mannoni
* @description Hides sticker if empty 
* @created Mon May 14 2018 13:11:29 GMT+0100 (BST)
* @copyright 
* @last-modified Mon May 14 2018 13:11:29 GMT+0100 (BST)
*/
 
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.donation_sticker = {
    attach: function(context) {
      if($('.field-name-field-sticker .content .field', context).length == 0){
        $('.donation-sticker').hide();
      }
    }
  };
})(jQuery, Drupal);
