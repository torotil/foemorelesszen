/**
* @file donation.tooltip.js
* @author Luigi Mannoni
* @description Drive the custom html tooltips for donation webforms
* @created Mon May 14 2018 13:11:29 GMT+0100 (BST)
* @copyright 
* @last-modified Mon May 14 2018 13:11:29 GMT+0100 (BST)
*/
  
(function ($, Drupal) {
  'use strict';
  
  Drupal.behaviors.donation_tooltip = {
    attach: function(context) {
      $('.tooltip', context).each(function(){
        var $tooltip = $(this);
        var $parent = $tooltip.parent();
        var counter = 0;
        
        while(!$parent.hasClass('webform-component-markup')) {
          $parent = $parent.parent();
          counter++;
          if (counter > 10) {
            // giving up, reassign original value
            $parent = $tooltip.parent();
            return;
          }
        }
        
        var $donationSelect = $parent.prev();

        setTimeout(function() {
          $donationSelect.find('.radio').each(function(i){
            var nth = i+1;
            if($(this).find('input:checked').length){
              $tooltip.find('span:nth-child('+nth+')').addClass('active');
              if (nth === 4) {
                $donationSelect.find('.form-type-textfield').show();
              }
            } else {
              $tooltip.find('span:nth-child('+nth+')').removeClass('active');
            }
            $(this).find('label').on('click', function(){
              $tooltip.find('.active').removeClass('active');
              $tooltip.find('span:nth-child('+nth+')').addClass('active');
            });
          });
        }, 10);
      });
    },
  };
})(jQuery, Drupal);
