/**
 * @file foe.js-tokens.js
 * @author Luigi Mannoni
 * @description Replaces {{ }} templates on DOM 
 */ 

'use strict';

(function($, Drupal, store) {
  /**
   * The actual behaviour, should trigger once on load/ajax load:
   */
  Drupal.behaviors.foe_js_tokens = {
    attach: function(context) {
      $('[data-form-token]', context).each(function() {
        var $this = $(this);
        var name = $this.attr('data-form-token');
        var value = store.getFoeTextField(name) || store.getWebformTextField(name);
        if (!!value) {
          $this.text( value.replace(/^"(.+(?="$))"$/, '$1') );
        }
      });
    },
  };
})(jQuery, Drupal, store);
