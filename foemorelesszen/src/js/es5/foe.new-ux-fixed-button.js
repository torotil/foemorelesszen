(function($, Drupal) {
    'use strict';

    function getScrollTop() {
        if (typeof pageYOffset !== 'undefined') {
            //most browsers except IE before #9
            return pageYOffset;
        } else {
            var B = document.body; //IE 'quirks'
            var D = document.documentElement; //IE with doctype
            D = (D.clientHeight) ? D : B;
            return D.scrollTop;
        }
    }

    Drupal.behaviors.foe_new_ux_fixed_btn = {
        attach: function(context, settings) {
            $('body', context).once(function() {
                // First ask "Yes" button      
                var $formContainer = $('aside.new-ux-sticky');

                var $formActionOverride = $('input[name="submitted[sticky_cta_override]"]');
                var $formAction = $formContainer.find('.form-actions').children('input[type="submit"]');

                var $buttonContainer = $('.new-ux-fixed-button');
                var $button = $buttonContainer.find('button');
                var mobilePos = 0;
                var desktopPos = 0;

                // Copy classes and text from the main form button
                $button.addClass($formAction.attr('class'));
                $button.text($formActionOverride.val() || $formAction.val());

                $(window, context).on('scroll resize load', function(event) {
                    if ($formContainer.length) {
                        mobilePos = $formContainer.offset().top + 340;
                        desktopPos = $('.form-actions').offset().top + 40;
                    }

                    var current = getScrollTop();
                    // Check bottom of screen
                    if (current + (window.innerHeight) >= mobilePos) {
                        $buttonContainer.addClass('slide-out-mobile');
                    } else {
                        $buttonContainer.removeClass('slide-out-mobile');
                    }

                    if (current < desktopPos) {
                        $buttonContainer.addClass('slide-out-desktop');
                    } else {
                        $buttonContainer.removeClass('slide-out-desktop');
                    }
                });

            });
        }
    };

})(jQuery, Drupal);