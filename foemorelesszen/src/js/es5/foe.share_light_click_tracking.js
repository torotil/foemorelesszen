/**
 * @file foe.share_light_click_track.js
 * @author James South
 * @description Adds data-click-track attributes to share light links, since we can't easily touch those module templates
 */ 

'use strict';

(function($, Drupal) {
  Drupal.behaviors.share_light_click_track = {
    attach: function(context) {

      $('.share-light a', context).each(function() {

        var action = this.dataset.share || "unknown";

        var clickTrackingData = {
          eventCategory: "share",
          eventAction: "thank-you-page-" + action + "-share",
          eventLabel: (this.title.trim() || this.textContent.trim()) + ' | ' + this.href
        };

        this.dataset.clickTrack = JSON.stringify(clickTrackingData);

      });
    },
  };
})(jQuery, Drupal);
