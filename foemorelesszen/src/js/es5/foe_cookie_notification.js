/**
 * @file foe_custom_event_tracking_helper.js
 *
 * Creates an agnostic helper function Foe.trackCustomEvent() to trigger tracking events across multiple tracking
 * platforms. By default it will send the event to all defined platforms, but an optional second argument can provide
 * a whitelist as an array of strings. "Whitelisted" platforms that do not exist will be silently ignored.
 *
 * Add platform function definitions to the 'platforms' object as needed, taking the eventName as a string parameter.
 */


Drupal.behaviors.foe_cookie_notification = {};
Drupal.behaviors.foe_cookie_notification.attach = function(context, settings) {

  var cookieName = 'cookieconsent_dismissed';
  var cookieDomain = '.friendsoftheearth.uk';
  var expiryDays = 183; // Six months

  if (context === document && !$.cookie(cookieName)) {

    // Create the cookie-consent elements
    // Kept as HTML here for ease of editing.
    var htmlString = '' +
        '<div class="foe_cc_content">' +
        '<span id="foe_cc_content__accept_button" class="foe_cc_content__button">Accept cookies</span>' +
        '<p class="foe_cc_content__message">' +
        'Our website uses cookies to understand how people interact with our content, which helps us ' +
        'offer the best experience for our visitors. By continuing to browse the site you are ' +
        'agreeing to our use of cookies.' +
        '<a target="_blank" class="foe_cc_content__link" href="https://friendsoftheearth.uk/about-us/about-cookies">' +
        '&gt;&gt; Read our cookie policy' +
        '</a>' +
        '</p>' +
        '</div>';

    var consentDiv = document.createElement('div');
    consentDiv.classList = 'foe_cc_wrapper';
    consentDiv.id = 'foe_cc_wrapper';
    consentDiv.innerHTML = htmlString;

    document.body.appendChild(consentDiv);

    // Add "accept" event handler
    $('#foe_cc_content__accept_button').click(function() {

      // Set the cookie on the current domain
      $.cookie(cookieName, '1', {
        path: '/',
        expires: expiryDays
      });

      // Set the cookie for the root FOE domain
      $.cookie(cookieName, '1', {
        domain: cookieDomain,
        path: '/',
        expires: expiryDays
      });

      $('#foe_cc_wrapper').remove();

    });

  }

};