 /**
 * @file form.input-validator.js
 * Checks for input sanity and appends a error/success class on fields.
 */
'use strict';

(function($, Drupal) {
  // Got it from the interwebz
  var mailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var postCodeRegex = /^((BFPO ?[0-9]{1,4})|(GIR ?0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9]) ?[0-9][ABD-HJLNP-UW-Z]{2}))$/;

  /**
   * Validates input and appends classes.
   * @constructor
   * @param {object} $input - The jQuery dom object for the input.
   * @param {object} $wrapper - The jQuery dom object for the form-group wrapper.
   */
  function validateInput($input, $wrapper) {
    // Clean classes
    if ( $wrapper.hasClass('pristine') && $input.val() != '' ) $wrapper.removeClass('pristine');
    if ( $wrapper.hasClass('field-success') ) $wrapper.removeClass('field-success');
    if ( $wrapper.hasClass('field-error') ) $wrapper.removeClass('field-error');

    // We are only going to check the required inputs.
    if ( $input.attr('required') || $input.hasClass('required') ) {
      if ( $input.attr('type') == 'email' ) {
        // Check first the email
        if ( mailRegex.test($input.val()) ) {
          $wrapper.addClass('field-success');
        } else {
          $wrapper.addClass('field-error');
        }
      } else if ( $input.hasClass('postcode') ) {
        // Check first the postcode
        if ( postCodeRegex.test($input.val()) ) {
          $wrapper.addClass('field-success');
        } else {
          $wrapper.addClass('field-error');
        }
      } else {
        if ( $input.val() != '' ) {
          $wrapper.addClass('field-success');
        } else {
          $wrapper.addClass('field-error');
        }
      }
    }

    if ($wrapper.hasClass('field-error') && $wrapper.hasClass('js-prefill-hidden')) {
      $wrapper.removeClass('js-prefill-hidden element-invisible');
      setTimeout(function() {
        $input.val('');
      }, 50);
    }
  }

  /**
   * The actual drupal behaviour, should trigger once on load/ajax load:
   */
  Drupal.behaviors.ajax_form_hints = {
    attach: function(context) {
      $('form.webform-client-form input', context).each(function() {
        // Get elements.
        var $input = $(this);
        var $wrapper = $(this).parent('.form-group');

        // Trigger this onload.
        validateInput($input, $wrapper);

        // Attach x-browser event for real time validation (do we really need <IE9 ?!?)
        $input.on('propertychange input change', function(e) {
            var valueChanged = false;

            if (e.type == 'propertychange') {
                valueChanged = e.originalEvent.propertyName == 'value';
            } else {
                valueChanged = true;
            }
            if (valueChanged) {
              if ($input.hasClass('postcode')) {
                $(this).val(function(_, val) {
                  return val.toUpperCase();
                });
              }
              validateInput($input, $wrapper);
            }
        });
        // On property change end
      });
      // Behaviour context function end
    },
  };
})(jQuery, Drupal);
