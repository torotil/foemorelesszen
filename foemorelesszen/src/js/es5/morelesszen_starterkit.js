(function($) {
Drupal.behaviors.morelesszen_starterkit = {};
Drupal.behaviors.morelesszen_starterkit.attach = function(context, settings) {
  // scroll to top of form + padding if we are below it
  // or if we are more than the toleratedOffset above it
  if ('ajax' in settings) {
    $.each(settings.ajax, function(el) {
      var padding = 12;
      var toleratedOffeset = 50;
      if (el && el.length > 0 && Drupal.ajax[el]) {
        var oldsuccess = Drupal.ajax[el].success;

        Drupal.ajax[el].success = function (response, status) {
          oldsuccess.call(this, response, status);
          var $wrapper = $('#' + settings.ajax[el].wrapper);
          var $html = $('html');
          var diff = Math.abs($wrapper.offset().top) - Math.abs($html.offset().top);
          if (diff < 0 || Math.abs(diff) > toleratedOffeset) {
            // $('html').animate({ scrollTop: ($wrapper.offset().top - padding)}, 'slow');
          }
        }
      }
    });
  }

  // container id begins with webform-ajax-wrapper
  $('.webform-client-form', context).webformAjaxSlide({
    slideSpeed: 3,
    loadingDummyMsg: Drupal.t('loading'),
    onSlideBegin: function (ajaxOptions) {},
    onSlideFinished: function (ajaxOptions) {},
    onLastSlideFinished: function (ajaxOptions) {}
  });
};

Drupal.behaviors.clickableTeasers = {};
Drupal.behaviors.clickableTeasers.attach = function(context, settings) {
  $('.node-teaser', context).click(function(event) {
    event.preventDefault();
    window.location.href = $('a.node-link', this).attr('href');
  }).addClass('clickable');
};

Drupal.behaviors.mobilemenu = {};
Drupal.behaviors.mobilemenu.attach = function(context, settings) {
  if ($.fn.mobilemenu) {
    // enable and configure the mobilemenu
    // for the full set of options see jquery.mobilemenu.js
    $('#main-menu', context).mobilemenu({
      breakpoint: 780, // same as @menu-breakpoint in menu.less
      dimBackground: true,
      dimElement: '.campaignion-dialog-wrapper',
      animationFromDirection: 'left'
    });
  }
};

Drupal.behaviors.payment_slide = {};
Drupal.behaviors.payment_slide.attach = function(context, settings) {
  /*
   * $selector slides out on click and $forms slide in.
   * $forms slide out on click on the back-link and $selector slides in.
   * $wrapper.height() is animated accordingly.
   */
  // only act on payment webform component wrappers.
  var behavior = this;

  $('.paymethod-select-wrapper', context)
    .css({position: 'relative'})
    .each(function() {
    // initial state: selector visible forms invisible
    var $wrapper = $(this);
    var $selectorWrapper = $wrapper.find('.paymethod-select-radios');
    var $selector = $wrapper.children('.form-type-radios');
    behavior.$selector = $selector;
    if ($selector.length <= 0)
      return;

    var $forms = $('.payment-method-all-forms', $wrapper);

    if (behavior.showForms) {
      $selector.css({left: '-120%', position: 'relative', top: 0, margin: 0}).hide();
      $forms.css({position: 'relative', right:'0%', top:0, margin: 0});
    } else {
      $selector.css({position: 'relative', top: 0, left: '0%', margin: 0});
      $forms.css({position: 'absolute', right: '0%', top: 0, margin: 0}).hide();
    }

    var $submit_buttons = $wrapper.parents('form').find('.form-actions').appendTo($forms);

    $selectorWrapper.find('label').click(function() {
      behavior.showForms = true;
      // slide in forms and select out
      $wrapper.height($selector.height());
      $selector.css({position: 'absolute', top: 0, left: '0%'})
      .animate({left: '-120%'}, 500, 'swing', function() {
        $selector.hide().css('position', 'relative');
      });

      $forms.show();
      $forms.css({position: 'absolute', width: '100%', right: '-120%'})
      .animate({right: '0%'}, 500, 'swing', function() {
          $forms.css('position', 'relative');
      });

      $wrapper.animate({height: $forms.height()}, 500, 'swing', function() {
        $wrapper.css({'height': 'auto', 'overflow': 'visible'});
      });
    });

    $('<div class="payment-slide-back"><a class="btn btn-primary btn-block btn-back" href="#">' + Drupal.t('Choose a different payment method') + '</a></div>')
    .prependTo($forms)
    .click(function(e) {
      behavior.showForms = false;
      //slide out forms and selector in.
      $selector.css({position: 'relative', width: '100%'});
      $selector.show().animate({left: '0%'}, 500, 'swing', function() {
        $selector.css('position', 'relative');
      });

      $wrapper.height($forms.height());
      $forms.css('position', 'absolute');
      $forms.animate({right: '-120%'}, 500, 'swing', function() {
        $forms.hide().css('position', 'relative');
      });

      $wrapper.animate({height: $selector.height()}, 500, 'swing', function() {
        $wrapper.css('height', 'auto');
      });
      // do not bubble
      e.stopPropagation();

      // return false to prevent a page reload
      return false;
    });
  });
};

// Reintegrate the UX_ behaviours here since we detached that on the template
Drupal.behaviors.campaignion_ux = {};
Drupal.behaviors.campaignion_ux.attach = function(context) {
  // generate an dialog
  // used fo graying out the content while using the "new" action
  if($('.campaignion-dialog-wrapper').length < 1) {
    var $dialog = $('<div class="campaignion-dialog-wrapper"><div class="camapignion-dialog-content"></div></div>');
    $dialog.appendTo($('body'));
  }
};

Drupal.behaviors.campaignion_ux_webform_ajax_scroll = {};
Drupal.behaviors.campaignion_ux_webform_ajax_scroll.attach = function(context) {
  // Scroll to top of the form + padding if we are below or more than the
  // toleratedOffset above it.
  var padding = 50;
  var toleratedOffset = 0;
  if ($(context).is('[id^=webform-ajax-wrapper]')) {
    // This is the result of an AJAX submit.
    var $wrapper = $(context).parent().parent().parent();
    var wrapperTop = $wrapper.offset().top;
    var diff = wrapperTop - $(document).scrollTop();
    if (diff < 0 || diff > toleratedOffset) {
      $('body, html').animate({ scrollTop: (wrapperTop - padding)}, 'slow');
    }
  }
};

})(jQuery);
