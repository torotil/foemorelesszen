/**
 * @file pca_trigger.js
 * Simple helper to trigger a PCA.load when we encounter a set of ID fields during ajax load.
 * The IDs match the same IDs have been given to PCA, since I can't access their encapsulated config variable I've hardcoded them in here
 */

(function ($) {

  Drupal.behaviors.pca_trigger = {
    attach: function(context) {

      var allAddressElementNames = [
        'submitted[postcode]',
        'submitted[address_line_1]',
        'submitted[address_line_2]',
        'submitted[city]',
      ];

      var foundAddressElementNames = [];

      $('form.webform-client-form input', context).each(function() {
        if (allAddressElementNames.indexOf(this.name) !== -1) {
          foundAddressElementNames.push(this.name);
          $(this).attr('autocomplete', 'off');
        }
      });

      // Dedupe "found" array (SHOULD never be needed since input names SHOULD be unique, but let's make sure)
      foundAddressElementNames = foundAddressElementNames.filter(function(item, pos, self) {
        return self.indexOf(item) == pos;
      });

      if (foundAddressElementNames.length === allAddressElementNames.length && window.pca && window.pca.load) {
        /* Now hardcoded into html.tpl.php in order to run on the first instance */
        // window.pca.on('load', function(_type, _id, control) {
        //   window.pca.currentControl.destroy();
        //   window.pca.currentControl = control;
        //   control.listen('populate', function(_address) {
        //     allAddressElementNames.forEach( function (elementName) {
        //       $('[name="submitted[' + elementName + ']"]').change();
        //     });
        //   });
        // });
        window.pca.load();
      }

    }
  };

})(jQuery);
