/**
 * @file webform-foe-prefill-api.js
 * Pulls data from our API and slams it into our form.
 */
'use strict';

(function ($, Drupal, store) {

  /**
   * Handler function attached to form submit event
   * This sends 'submit' to Azure prefill service when we complete a form.
   */
  function submitToPrefill() {
    var userHash = store.getUserHash();
    var formHash = $('form').attr('hash');
    var formId = $('form').attr('id');
    var endpoint = Drupal.settings.prefillEndpoint;

    $.ajax({
      url: endpoint + '/api/v1/form/submit/',
      type: 'POST',
      dataType: 'json', // expected format for response
      contentType: 'application/x-www-form-urlencoded',
      data: 'hash=' + userHash + '&nid=' + formId + '&chk=' + formHash,
    });
  }


  /**
   * Helper function to hide opt in a specific channel
   * if the main related field has been prefilled
   *
   * @constructor
   * @param {object} name - Name of the opt in.
   */

  function hideOptIn(name) {
    var $inputs = $('form').find('[data-form-prefill-write="' + name + '"]');

    $inputs.each(function () {
      var $input = $(this);
      var $wrapper = $input.parents('.webform-component');
      if ($wrapper.hasClass('field-is-required')) {
        var val = $input.attr('value');

        switch (val) {
          case 'opt-in':
          case '1':
          case 'yes':
          case 'HY':
          case 'true':
            $input.prop('checked', true).trigger('change');
            $wrapper.addClass('js-prefill-hidden element-invisible');
            var $hidden = $('form').find('[data-form-prefill-write="' + name + '_set_by_prefill"]');
            $hidden.val('1');
            break;
        }
      } else {
        $wrapper.addClass('js-prefill-hidden element-invisible');
      }
    });
  }



  /**
   * Process the form, putting retrieved values into corresponding fields.
   * @constructor
   * @param {object} $form - The jQuery dom object.
   * @param {object} data - The Data to inject object.
   */

  function processForm($form, data) {

    // Enable prefill session
    $form.formPrefill({ prefix: 'foe_prefill', map: {} });

    // Hide specified elements if prefill is running (via custom CSS class 'foe-prefill-hide')
    // Also make affected inputs read-only, to disable browser auto-completion
    $form.find('.foe-prefill-hide').addClass('js-prefill-hidden element-invisible');
    $form.find('.foe-prefill-hide input, input.foe-prefill-hide').prop('readonly', true);

    var contactName = data['first_name'] || data['last_name'];
    var $inputs = $form.find('input');

    // Work out if this form requests a full postal address
    // (In which case we won't pre-fill/hide ANY address elements because
    // we currently only have postcode and city values, and that's WEIRD)
    var fullAddressInputCount = 0;
    var fullAddressInputIdArray = [
      'submitted[postcode]',
      'submitted[address_line_1]',
      'submitted[address_line_2]',
      'submitted[city]',
    ];
    $inputs.each(function () {
      fullAddressInputCount += (fullAddressInputIdArray.indexOf(this.name) > -1);
    });

    var fullAddressRequested = fullAddressInputCount === fullAddressInputIdArray.length;

    // Process each input
    $inputs.each(function () {

      var $input = $(this);
      var $wrapper = $input.parents('.webform-component');
      var key = $input.attr('data-form-prefill-write');

      // Bail out if we don't have a prefill-write key
      if (typeof key !== "string" || key.length === 0) {
        return;
      }


      // Bail out on specific blacklisted keys
      var blacklist = [
        'url', // Honeypot
        'gift_aid', // Gift aid opt-in
      ];
      if (blacklist.includes(key)) {
        return;
      }


      // Bail out on any non-whitelisted 'hidden' and 'submit' input types
      if (/hidden|submit/.test($input.attr('type'))) {
        var whitelist = [
          'contact_number'
        ];
        if (!whitelist.includes(key)) {
          return;
        }
      }


      // Bail out if explicitly and deliberately excluded via a custom
      // 'foe-webform-skip' CSS class on the input's wrapper
      if ($wrapper.hasClass('foe-prefill-skip')) {
        return;
      }

      // Bail out on all dp/opt-in radios/checkboxes
      if (key.indexOf('dp_') !== -1 ||
        key.indexOf('_opt_in') !== -1 ||
        key.indexOf('newsletter_subscription') !== -1) {
        return;
      }

      // If the current input DOESN'T have data but IS required
      // then don't prefill it ('coz we can't) but also don't hide it.
      if (!data[key] && !$input.val() && $wrapper.hasClass('field-is-required')) {
        return;
      }

      // Also bail on required radio inputs with nothing selected
      if ($wrapper.hasClass('field-is-required') &&
        $input.attr('type') === 'radio' &&
        $form.find('input[name="submitted[' + key + ']"]:checked').length === 0) {
        return;
      }

      // Don't partially pre-fill full addresses, it's weird. (And messes with the
      // postcode lookup UX because users won't type their postcode if it's
      // pre-filled, and then the lookup function doesn't trigger as expected.)
      //
      // TODO: Prefill whole addresses...?
      // TODO: Deal with reloads better (sessionStorage data gets loaded in regardless)
      if (fullAddressRequested) {
        if (key === 'postcode' || key === 'address_line_1' || key === 'address_line_2' || key === 'city') {
          return;
        }
      }

      // If the current input DOESN'T have data and IS NOT required
      // and DOES NOT HAVE the custom 'foe-prefill-show' CSS class
      // then hide and bail out.
      if (!data[key] && !$wrapper.hasClass('field-is-required') && !$wrapper.hasClass('foe-prefill-show')) {
        $wrapper.addClass('js-prefill-hidden element-invisible')
          .find('input').prop('readonly', true);
        switch (key) {
          case 'email':
            hideOptIn('email_opt_in');
            hideOptIn('newsletter_subscription');
            break;
          case 'postcode':
            hideOptIn('post_opt_in');
            break;
          case 'phone_number':
            hideOptIn('phone_opt_in');
            break;
        }
        return;
      }

      // Now, if we have a data value (and prefill hasn't been skipped via one
      // of the rules above) then the main event happens
      if (data[key] || $input.val()) {

        // Visually hide the wrapper element and make the input read-only to disable browser auto-complete
        // (...unless instructed NOT to hide it by the custom 'foe-prefill-show' CSS class.)
        if (!$wrapper.hasClass('foe-prefill-show')) {
          $wrapper.addClass('js-prefill-hidden element-invisible');
          $input.prop('readonly', true);
        }

        // Inject the prefill data, unless there's an existing value. (Either
        // URL-fragment supplied, or from a previous form step or submission
        // within this session, and so can be assumed to be up-to-date.)
        //
        // Override and force the foe-prefill value if the wrapper has custom
        // CSS class 'foe-prefill-force'
        if (!$input.val() || $wrapper.hasClass('foe-prefill-force')) {
          $input.val(data[key]);
        }

        // Broadcast a change
        $input.trigger('change');

        // Knock-on effects of pre-filling certain keys
        switch (key) {

          case 'email':
            hideOptIn('email_opt_in');
            hideOptIn('newsletter_subscription');
            break;

          case 'postcode':
            hideOptIn('post_opt_in');
            break;

          case 'phone_number':
            hideOptIn('phone_opt_in');
            break;
        }

      }
    });


    // Parse the salutation override
    var $salutationOverride = $form.find('input[name="submitted[prefill_salutation_override]"]');

    if ($salutationOverride.length && $salutationOverride.val() !== '') {
      var parsedSalutation = $salutationOverride.val().replace('!name', '<span class="js-prefill-name">Friend</span>');
      $('.prefill-salutation').children('h3').html(parsedSalutation);
    }

    // And greet the user
    $('.js-prefill-name').html(contactName);
    $('.prefill-salutation').removeClass('element-invisible');

    var existingOptIns = [];
    var optIns = ['email_opt_in', 'newsletter_subscription', 'post_opt_in', 'phone_opt_in'];
    optIns.forEach(function (element) {
      var $optin = $('form').find('[data-form-prefill-write="' + element + '"]');
      if ($optin.length > 0) {
        existingOptIns.push(element);
      }
    });

    var clonedOptIns = Array.from(existingOptIns);

    existingOptIns.forEach(function (element) {
      var $optin = $('form').find('[data-form-prefill-write="' + element + '"]');
      var $wrapper = $optin.closest('.form-item');
      if ($wrapper.hasClass('js-prefill-hidden')) {
        clonedOptIns.shift();
      }
      if (clonedOptIns.length === 0) {
        $('.webform-component--consent-fields').addClass('js-prefill-hidden element-invisible');
      }
    });

    // Intercept the form submit and send the data to the Prefill
    // Note: Preventing the event and submitting after the POST
    //       it will bug out the default webform behaviour and starts
    //       to send thousands of requests per second.
    // Maybe todo: See if we can extend the webform submit behaviour
    //             (which I need to dig out the documentation)
    $form.on('submit', submitToPrefill);

    showForm();
  }


  /**
   * Clears the form and removes salutation.
   */
  function clearForm() {
    var $form = $('form');

    // Show any fields that were hidden and re-enable browser autocompletion
    $('form').find('.js-prefill-hidden input, input.js-prefill-hidden').prop('readonly', false);
    $('form').find('.js-prefill-hidden').removeClass('js-prefill-hidden element-invisible');

    $form.off('submit', submitToPrefill);

    var $inputs = $('form').find('input');

    $inputs.each(function () {
      // We are not using a quicker .parent().hasClass() here because we try to replicate
      // the module behaviour that runs when 'change' event is triggered.
      var $input = $(this);

      // Skip webform navigation/submission buttons
      if ($input.hasClass('form-submit')) {
        return;
      }

      var $triggerElement = $input.closest('.webform-component');

      // Make sure both elements and method are not undefined
      if (typeof $triggerElement !== 'undefined' && typeof $triggerElement.attr('class') !== 'undefined') {
        if ($triggerElement.attr('class').match(/webform-component--[^ ]+/)[0]) {
          if ($input.attr('type') === 'radio' || $input.attr('type') === 'checkbox') {
            $input.prop('checked', false).trigger('change').addClass('pristine');
          } else {
            $input.val('').trigger('change').addClass('pristine');
          }

          var name = $input.attr('data-form-prefill-write');
          if (name && name.indexOf('_set_by_prefill') > 0) {
            $input.val('');
          }
        }
      }
    });


    $('.prefill-salutation').addClass('element-invisible');
    store.destroy(true);

    // Unset all errors
    setTimeout(function () {
      $('.field-error').removeClass('field-error');
      $('.field-success').removeClass('field-success');
    }, 50);

    // Clear all prefill sessionStorage items
    for (var i = 0; i < sessionStorage.length; i++) {
      if (/_prefill:/.test(sessionStorage.key(i))) {
        sessionStorage.removeItem(sessionStorage.key(i));
      }
    }

    // Add a sessionStorage disabling flag to prevent FOE prefill from running again in this session
    sessionStorage.setItem('foe-prefill-reset', 1);

    // Revert prefill storage to more onion's default
    $form.formPrefill({ prefix: 'webform_prefill' });

    showForm();
  }

  function showForm() {
    setTimeout(function () {
      $('form').removeClass('foe_webform_prefill_get-active');
    }, 250);
  }




  /**
   * The actual prefill behaviour, should trigger once on load/ajax load:
   */
  Drupal.behaviors.foe_webform_prefill_get = {

    attach: function (context) {

      // Bail out if we've got a reset flag. NO PREFILL FOR YOU.
      if (sessionStorage.getItem('foe-prefill-reset')) {
        return;
      }

      // Run the campaignion prefill first, to pick up existing values. (They'll
      // either be URL-fragment supplied, or from a previous form step or submission
      // within this session, and so can be assumed to be up-to-date.)
      if (Drupal.behaviors.webform_prefill && typeof Drupal.behaviors.webform_prefill.attach === 'function') {
        Drupal.behaviors.webform_prefill.attach(context);
      }

      $('form', context).each(function () {
        var $form = $(this);
        var data = null;
        var endpoint = Drupal.settings.prefillEndpoint;

        // As soon we boot up we'll check if we have a hash on URL
        var userHash = store.getUserHash();
        var formHash = $form.attr('hash');
        var formId = $form.attr('id');

        if (userHash) {
          $form.addClass('foe_webform_prefill_get-active');

          // Exit if we're in a errored/processed form
          if ($('.alert.alert-danger').length) {
            setTimeout(clearForm, 120);
            return;
          }

          data = store.getData();

          if (data !== null && data.urn_hash === userHash) {
            processForm($form, data);
          } else {
            store.destroy(true);
            $.ajax({
              url: endpoint + '/api/v1/form/prefill/',
              type: 'POST',
              dataType: 'json', // expected format for response
              contentType: 'application/x-www-form-urlencoded',
              data: 'hash=' + userHash + '&nid=' + formId + '&chk=' + formHash,
              timeout: 3000,
            })
              .done(function (result) {
                if (typeof result.data !== 'undefined') {
                  result.data.urn_hash = userHash;
                  store.setData(result.data);
                  data = store.getData();
                  setTimeout(processForm, 120, $form, data);
                }
              })
              .always(function () {
                showForm();
              });
          }
        }
      });
    },
  };



  /**
   * The reset form behaviour, hanging off a 'js-prefill-reset' CSS class
   * If we're past the first step of a multi-step form, causes a page refresh
   */
  Drupal.behaviors.foe_webform_prefill_reset = {
    attach: function (context) {
      $('.js-prefill-reset', context)
        .on('click', function (event) {
          event.preventDefault();
          clearForm();

          if (!document.querySelectorAll('.webform-progressbar > .webform-progressbar-page.current:first-child').length) {
            // We've got a progress bar, and we're NOT on the first step of it
            // We reload the page, and fade out the form while we do so as a vague UX nicety
            var el = document.querySelector('.petition-sidebar');
            if (el) {
              el.style.transition = 'opacity 300ms ease-in-out';
              el.style.opacity = 0;
            }
            window.location.reload();
          }
        });
    },
  };

})(jQuery, Drupal, window.store);