/*
 * @file webform-tracking.js
 * @project foemorelesszen
 * @description This file catches the URL parameters and feeds them
 *              into the form using the prefill attribute. 
 * 
 * @created Thursday, 28th June 2018 1:00:52 pm
 * 
 * @last-modified Thursday, 28th June 2018 3:51:40 pm
 *                Luigi Mannoni (luigi.mannoni@foe.co.uk)
 */

(function($, Drupal) {
  'use strict';

  Drupal.behaviors.foe_webform_tracking = {
    field_map: {
      "external_referrer" : "foe_external",
      "source"            : "foe_source",
      "medium"            : "foe_channel",
      "term"              : "foe_term",
      "content"           : "foe_content",
      "campaign"          : "foe_campaign",
    },
    extra_parameters: {
      "source"   : "s",
      "medium"   : "m",
      "version"  : "v",
      "other"    : "o",
      "term"     : "t",
      "campaign" : "c",
      "refsid"   : "refsid",
    },
    google_analytics: {
      "source"   : "utm_source",
      "medium"   : "utm_medium",
      "term"     : "utm_term",
      "content"  : "utm_content",
      "campaign" : "utm_campaign",
    },

    attach: function(context) {
      // Run only once per page-load.
      if (context == document) {
        this.run();
      }
    },

    // Get white listed parameters.
    extract_parameters: function(parameters) {
      var data = {};
      var map = this.extra_parameters;
      for (var key in map) {
        var candidates = [key, map[key], this.google_analytics[key]];

        // Take first candidate key with a value.
        for (var i = 0, l = candidates.length; i < l; i++) {
          var value = parameters[candidates[i]];
          if (value) {
            data[key] = value;
            break;
          }
        }
      }
      return data;
    },

    // Adds the URL to a history array (if needed).
    history_add: function(history, url) {
      if (history[0] != url) {
        history.unshift(url);
      }
    },

    run: function() {
      var history_limit = 5;

      var tracking_data = $.extend({
        history: [],
        tags: [],
      }, JSON.parse($.cookie('foe_tracking')) || {});
      var parameters = this.get_url_parameters();
      var base_url = Drupal.settings.webform_tracking.base_url;

      tracking_data.user_id = tracking_data.user_id || this.new_user_id();

      // tags
      var tags = tracking_data.tags || [];
      if (typeof parameters['tag'] !== 'undefined') {
        $.merge(tags, parameters['tag'].split(','));
      }
      tracking_data.tags = this.sort_unique(tags);

      // extra parameters
      $.extend(tracking_data, this.extract_parameters(parameters));

      if (typeof tracking_data['external_referer'] === 'undefined') {
        if (document.referrer.indexOf(base_url) !== 0) {
          tracking_data['external_referer'] = document.referrer;
        }
        else if (typeof parameters['external_referer'] !== 'undefined' && parameters['external_referer'].indexOf(base_url) !== 0) {
          tracking_data['external_referer'] = parameters['external_referer'];
        }
      }
      
      tracking_data['nid'] = $('article[data-nid]').attr('data-nid');
      this.history_add(tracking_data.history, window.location.href);
      if(tracking_data.history.length > history_limit) {
        tracking_data.history = tracking_data.history.slice(0, history_limit);
      }

      this.populate_foe_tracking(tracking_data);
      $.cookie('foe_tracking', JSON.stringify(tracking_data), {path: '/'});
    },

    get_url_parameters: function() {
      var parameters = {};
      var variables = window.location.search.substring(1).split('&');
      for (var i = 0; i < variables.length; i++) {
        var parameter = variables[i].split('=');
        parameters[parameter[0]] = parameter[1];
      };
      return parameters;
    },

    new_user_id: function() {
      // http://x443.wordpress.com/2012/03/18/adler32-checksum-in-javascript/
      var adler32 =  function(a,b,c,d,e,f) {
        for (b=65521,c=1,d=e=0;f=a.charCodeAt(e++); d=(d+c)%b) c=(c+f)%b;
        return(d<<16)|c
      }
      return adler32(String(Math.random() + Date.now()));
    },

    sort_unique: function(array) {
      if (!array.length) {
        return array;
      }
      array = array.sort(function (a, b) { return a - b; });
      var result = [array[0]];
      for (var i = 1; i < array.length; i++) {
        if (array[i-1] !== array[i]) {
          result.push(array[i]);
        }
      }
      return result;
    },

    populate_foe_tracking: function(data) {    
      var json_data = JSON.stringify(data);
      var $prefill = $('input[data-form-prefill-write="foe_additional_data"]');
      var $legacy = $('input[name="submitted[foe_additional_data]"]');
      
      if ($prefill.length) {
        $prefill.val(json_data);
      } else {
        $legacy.val(json_data);
      }
    }

  }

})(jQuery, Drupal);
