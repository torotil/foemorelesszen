import { TimelineLite, TweenLite } from 'gsap/TweenMax';

export default {
  /**
   * Swap logic between two slides, animates them and stuff
   */
  swap: function swap($prev, $next) {
    const Q = window.FoeQuiz;
    const $ = window.jQuery;

    // Inhibit current slide
    $prev.css('pointer-events', 'none');

    // Get our images
    const $image = {
      prev: $prev.find('.question-img'),
      next: $next.find('.question-img'),
    };

    const isFirst = $prev.hasClass('quiz-slide-start');
    const isLast = $next.hasClass('quiz-slide-end');

    // GSAP Slide timeline animations
    // For options refer to: https://greensock.com/docs/TimelineLite
    // All timing values are in seconds (meh), or can add `useFrames: true` to the
    // option objects below, but results might differ in browsers/machines (optimally they run at 60fps)

    const tl = new TimelineLite({ paused: true, align: 'normal', delay: 0 });
    const animDuration = 0.25;

    // Swap Handlers
    const onCompletePrev = () => {
      // Start our game
      if (isFirst) {
        Q.$ui.canvas.addClass('quiz-started');
      }

      $prev.addClass('d-none');
    };

    const onStartNext = () => {
      $next.removeClass('d-none');
      if ($image.next.length > 0) {
        Q.$ui.bg.css('background-image', `url(${$image.next.find('img').attr('src')}`);
      } else {
        // Restore original css value
        Q.$ui.bg.css('background-image', Q.$ui.bg.img);
      }

      if (isFirst) {
        TweenLite.to($('.quiz-only'), animDuration, { opacity: 1 });
        $(document).trigger('foequiz:start');
      }

      if (isLast) {
        TweenLite.to($('.quiz-only'), animDuration, { opacity: 0 });
        Q.$ui.canvas.removeClass('quiz-started bg-extradark').addClass('quiz-ended bg-offwhite');
        TweenLite.to(Q.$ui.canvas.find('.foe-logo-white'), animDuration, { opacity: 0 });
        TweenLite.to(Q.$ui.canvas.find('.foe-logo-negative'), animDuration, { opacity: 1 });
        $(document).trigger('foequiz:end');
      } else {
        $(document).trigger('foequiz:progress');
      }
    };

    // Timeline build
    if ($image.prev !== null && !isFirst) {
      tl.to(Q.$ui.bg, animDuration, { opacity: 0 })
        .to($image.prev, animDuration * 2, { opacity: 0, y: -40 });
    }

    tl.to($prev, animDuration, { opacity: 0, onComplete: onCompletePrev })
      .add('slide-out')
      .set(Q.$ui.bg, { y: 100, opacity: 0 })
      .from($next, animDuration, { opacity: 0, onStart: onStartNext })
      .add('slide-in')
      .to(Q.$ui.bg, animDuration * 2, { opacity: 1 });

    if ($image.next !== null && !isLast) {
      tl.from($image.next, animDuration * 2, { opacity: 0, y: 40 }, 'slide-in')
        .to(Q.$ui.bg, 15, { y: -100 }, 'slide-in');
    }

    // Trigger our animation
    tl.play();
  },

  /**
   * Selects the next slide and triggers a swap
   */
  next: function next() {
    const Q = window.FoeQuiz;
    const step = Q.questions.current;
    const $ = window.jQuery;

    let $1 = null;
    let $2 = null;

    if (Q.status === 'ready') {
      $1 = $('.quiz-slide-start');
      $2 = Q.$ui.slides.first();
    } else if (step === (Q.questions.total - 1)) {
      $1 = Q.$ui.slides.last();
      $2 = $('.quiz-slide-end');
    } else {
      $1 = $(Q.$ui.slides.get(step));
      $2 = $(Q.$ui.slides.get(step + 1));
      Q.questions.current += 1;
    }

    exports.default.swap($1, $2);
  },

  /**
   * Selects the previous slide and triggers a swap
   * (Currently not used)
   */
  previous: function previous() {
    const Q = window.FoeQuiz;
    const step = Q.questions.current;
    const $ = window.jQuery;

    let $1 = null;
    let $2 = null;

    if (step === 0) {
      $1 = Q.$ui.slides.first();
      $2 = $('.quiz-slide-start');
    } else if (Q.status === 'end') {
      $1 = $('.quiz-slide-end');
      $2 = Q.$ui.slides.last();
    } else {
      $1 = $(Q.$ui.slides.get(step));
      $2 = $(Q.$ui.slides.get(step - 1));
      Q.questions.current -= 1;
    }

    exports.default.swap($1, $2);
  },

};
