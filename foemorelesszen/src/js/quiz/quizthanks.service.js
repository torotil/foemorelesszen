import Screen from '../utils/screen.service';
import Cookie from '../utils/cookie.service';

export default {
  init: function init() {
    const Q = window.FoeQuiz;
    const userCookie = Cookie.get(`quiz-${Q.id}`);
    if (userCookie) {
      Q.answers = JSON.parse(userCookie);
    }

    if (Q.answers.length === Q.data.length) {
      exports.default.scoreCalculate();
    } else {
      exports.default.scoreInvalidate();
    }
  },

  scoreCalculate: function scoreCalculate() {
    const $ = window.jQuery;
    const Q = window.FoeQuiz;
    const $cards = $('.score-card-container');
    let score = 0;
    let share_data, share_data_encoded;

    $cards.each((index, el) => {
      const $this = $(el);
      const userAnswer = Q.answers[index];
      const { correctAnswer, answers } = Q.data[index];

      if (parseInt(userAnswer, 10) === correctAnswer) {
        score += 1;
        $this.find('.js-score-card-wrong').hide();
      } else {
        $this.find('.js-score-card-correct').hide();
      }

      $this.find('.js-score-card-answer-user').text(answers[userAnswer].text);
    });

    Q.$ui.canvas.find('.js-score-result').text(score);

    share_data = btoa(JSON.stringify({'score':score}));
    share_data_encoded = encodeURIComponent(share_data);

    Q.$ui.canvas.find('#share a[href]').each((index, el) => {
      el.href = el.href.replace('__share_data__', share_data);
      el.href = el.href.replace('__share_data_encoded__', share_data_encoded);
      el.dataset.clickTrack = el.dataset.clickTrack.replace('__share_data__', share_data);
      el.dataset.clickTrack = el.dataset.clickTrack.replace('__share_data_encoded__', share_data_encoded);
    });

  },

  scoreInvalidate: function scoreInvalidate() {
    const $ = window.jQuery;
    const $cards = $('.score-card-container');

    $cards.each((index, el) => {
      const $this = $(el);
      $this.find('.js-score-card-correct').hide();
      $this.find('.js-score-card-answer-user').parent().hide();
    });
  },

  recalculate: function recalculate() {
    const $ = window.jQuery;
    const Q = window.FoeQuiz;

    $('.fs-slide').each((index, el) => {
      const top = el.offsetTop;
      const height = el.clientHeight;
      const bottom = top + height;

      Q.thanks[el.id] = {
        index,
        el,
        top,
        bottom,
        visible: false,
      };
    });
  },

  reveal: function reveal() {
    const $ = window.jQuery;
    const Q = window.FoeQuiz;
    const headerHeight = $('header.foe-header').height();
    const ids = $(this).attr('foe-thanks-reveal').split(' ');
    let $slide;

    ids.forEach( (id) => {
      $slide = $(Q.thanks[id].el);
      if ($slide.hasClass('d-none')) {
        $slide.removeClass('d-none').addClass('d-flex');
      }
    });

    // Retrigger an element recalculate
    exports.default.recalculate();

    // Now trigger the animation.
    $('html, body').animate({
      scrollTop: Q.thanks[ids[0]].top + headerHeight
    }, 500);
  },

  watch: function watch() {
    const $ = window.jQuery;
    const Q = window.FoeQuiz;

    const scroll = Screen.getScrollTop();

    Object.entries(Q.thanks).forEach(([key, item]) => {
      if ($(item.el).hasClass('d-none')) {
        Q.thanks[key].visible = false;
      } else {
        const viewHeight = Screen.getViewportHeight() / 2; // Gives 50% screen height of threshold
        Q.thanks[key].visible = !(item.bottom < scroll || item.top - viewHeight >= scroll);
      }
    });

    // Challenge button...
    if ((Q.thanks.breakdown.visible && !Q.thanks.share.visible) && !Q.$ui.challenge.hasClass('in-view')) {
      Q.$ui.challenge.addClass('in-view');
    }

    if ((!Q.thanks.breakdown.visible || Q.thanks.share.visible) && Q.$ui.challenge.hasClass('in-view')) {
      Q.$ui.challenge.removeClass('in-view');
    }
  },
};
