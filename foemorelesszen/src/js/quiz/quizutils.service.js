import { TweenLite } from 'gsap/TweenMax';
import Cookie from '../utils/cookie.service';

export default {
  /**
   * This bad boy updates the progress on the top of our quiz.
   */
  progress: function progress() {
    const Q = window.FoeQuiz;

    const step = Q.questions.current + 1;
    Q.$ui.progress.bar.css('width', `${(100 / Q.questions.total) * step}%`);
    Q.$ui.progress.step.html(step);
  },

  /**
   * Cycles through the json and preloads all the images before starting the quiz.
   */
  preload: function preload() {
    const Q = window.FoeQuiz;
    const $ = window.jQuery;

    let imagesLoaded = 0;

    // Helper promise function :)
    const resolveLoad = img => new Promise((res) => { img.onload = res; });

    Q.data.forEach((item, index) => {
      const $slide = $(Q.$ui.slides.get(index));
      const $img = $slide.find('.question-img img');

      $img[0].src = item.question.image;

      resolveLoad($img[0]).then(() => {
        // Image have loaded.
        imagesLoaded += 1;
        Q.$ui.loader.bar.css('width', `${(100 / Q.questions.total) * imagesLoaded}%`);

        // When done
        if (imagesLoaded === Q.questions.total) {
          // Swap classes
          setTimeout(() => {
            Q.$ui.loader.button.removeClass('btn-outline-donate').addClass('btn-accent');
            TweenLite.to(Q.$ui.loader.bar, 0.25, {
              opacity: 0,
              onComplete: () => {
                Q.status = 'ready';
                Q.$ui.loader.button.on('click', () => { $(document).trigger('foequiz:slide.next'); });
                Q.$ui.loader.step.html('Start!');
              },
            });
          }, 500);
        }
      });
    });
  },

  /**
   * Starts the quiz.
   */
  start: function start() {
    const Q = window.FoeQuiz;
    Q.time.start = Date.now();
    Q.status = 'playing';
  },

  /**
   * Ends the quiz.
   */
  end: function end() {
    const Q = window.FoeQuiz;

    // Timer
    Q.time.stop = Date.now();
    Q.time.total = (Q.time.stop - Q.time.start) / 1000;

    Q.status = 'end';

    // Store answers on cookie with the associated node ID, so we can
    // store multiple quizzes. PS: I'd like this to be localstorage based tho.

    Cookie.set(`quiz-${Q.id}`, Q.answers);
  },

  /**
   * Registers an answer on the current slide and registers it on
   * the global object.
   */
  answer: function answer() {
    const Q = window.FoeQuiz;
    const $ = window.jQuery;

    // Register the answer
    const $button = $(this);
    $button.removeClass('btn-outline-offwhite').addClass('btn-accent');
    Q.answers.push($button.attr('foe-quiz-answer'));

    $(document).trigger('foequiz:slide.next');
  },
};
