<?php
/**
 * @see hook_css_alter().
 * We unset some of the Drupal's default CSS which are more harm than good for our theme
 *
 * Switch comment on/off as needed.
 * You can add new lines intercepting modules and stuff which try to bloat/override your CSS
 */
function foemorelesszen_css_alter(&$css) {

  // Remove known modules
  unset($css[drupal_get_path('module', 'system') . '/system.messages.css']);

  // Remove unwanted moreonion behaviour
  $blacklisted = [
    'css/jquery.fs.picker.css',
    'css/jquery.fs.selecter.css',
  ];
  foreach ($css as $path => $script) {
    foreach ($blacklisted as $blacklist) {
      if (stripos($path, $blacklist) !== FALSE) {
        unset($css[$path]);
      }
    }
  }
}

function foemorelesszen_js_alter(&$js) {

  // Remove known modules
  unset($js[drupal_get_path('module', 'campaignion_ux') . '/js/campaignion_ux.js']);

  // Remove unwanted moreonion behaviour
  $blacklisted = [
    'js/jquery.fs.picker.js',
    'js/jquery.fs.selecter.js',
  ];
  foreach ($js as $path => $script) {
    foreach ($blacklisted as $blacklist) {
      if (stripos($path, $blacklist) !== FALSE) {
        unset($js[$path]);
      }
    }
  }
}


// Watch at me while I do copy and paste some code from a project of 4 years ago
function _foemorelesszen_favicon_meta() {
  $folder = '/' . path_to_theme() . '/favicon/';

  $favicon_list = array(
    'favicon-apple' => array('tag' => 'link', 'rel' => 'apple-touch-icon', 'sizes' => '180x180', 'href' => $folder . 'apple-touch-icon.png'),
    'favicon-32' => array('tag' => 'link', 'rel' => 'icon', 'type' => 'image/png', 'sizes' => '32x32', 'href' => $folder . 'favicon-32x32.png'),
    'favicon-16' => array('tag' => 'link', 'rel' => 'icon', 'type' => 'image/png', 'sizes' => '16x16', 'href' => $folder . 'favicon-16x16.png'),
    'favicon-manifest' => array('tag' => 'link', 'rel' => 'manifest', 'href' => $folder . 'site.webmanifest'),
    'favicon-safari' => array('tag' => 'link', 'rel' => 'mask-icon', 'href' => $folder . 'safari-pinned-tab.svg', 'color' => '#34da96'),
    'favicon-tilecolor' => array('tag' => 'meta', 'name' => 'msapplication-TileColor', 'content' => '#1e234d'),
    'favicon-msconfig' => array('tag' => 'meta', 'name' => 'msapplication-config', 'content' => $folder . 'browserconfig.xml'),
    'favicon-themecolor' => array('tag' => 'meta', 'name' => 'theme-color', 'content' => '#1e234d'),
  );

  foreach ($favicon_list as $id => $fav) {
    $tag = $fav['tag'];
    unset($fav['tag']);
    drupal_add_html_head(array('#type' => 'html_tag', '#tag' => $tag, '#attributes' => $fav), $id);
  }
}


/**
 * Returns HTML for status and/or error messages, grouped by type.
 *
 * An invisible heading identifies the messages for assistive technology.
 * Sighted users see a colored box. See http://www.w3.org/TR/WCAG-TECHS/H69.html
 * for info.
 *
 * @param array $variables
 *   An associative array containing:
 *   - display: (optional) Set to 'status' or 'error' to display only messages
 *     of that type.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_status_messages()
 *
 * @ingroup theme_functions
 */
function foemorelesszen_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
    'info' => t('Informative message'),
  );

  // Map Drupal message types to their corresponding Bootstrap classes.
  // @see http://twitter.github.com/bootstrap/components.html#alerts
  $status_class = array(
    'status' => array(
      'class' => 'success',
      'icon' => 'fas fa-check-circle',
    ),
    'error' => array(
      'class' => 'danger',
      'icon' => 'fas fa-times-circle',
    ),
    'warning' => array(
      'class' => 'warning',
      'icon' => 'fas fa-exclamation-circle',
    ),
    // Not supported, but in theory a module could send any type of message.
    // @see drupal_set_message()
    // @see theme_status_messages()
    'info' => array(
      'class' => 'info',
      'icon' => 'fas fa-info-circle',
    ),
  );

  // Retrieve messages.
  $message_list = drupal_get_messages($display);

  // Allow the disabled_messages module to filter the messages, if enabled.
  if (module_exists('disable_messages') && variable_get('disable_messages_enable', '1')) {
    $message_list = disable_messages_apply_filters($message_list);
  }

  foreach ($message_list as $type => $messages) {
    $class = (isset($status_class[$type]['class'])) ? $status_class[$type]['class'] : 'info';
    $icon = (isset($status_class[$type]['icon'])) ? $status_class[$type]['icon'] : 'far fa-check-circle';

    $output .= '<div class="drupal-messages alert alert-dismissable border-' . $class . '"" role="alert">';
    $output .= '<div class="heading-container bg-' . $class . '"><i class="' . $icon . '"></i></div>';
    $output .= '<div class="message-container text-' . $class . '">';

    if (!empty($status_heading[$type])) {
      $output .= '<h4 class="element-invisible">' . filter_xss_admin($status_heading[$type]) . '</h4>';
    }

    foreach ($messages as $message) {
      $output .= '<p>' . filter_xss_admin($message) . '</p>';
    }

    $output .= '<button class="btn btn-block btn-' . $class . '" data-dismiss="alert" aria-label="Close">Close</button>';
    $output .= '</div>';

    $output .= '</div>';
  }
  return $output;
}


/**
 * @see theme_menu_local_tasks()
 */
function foemorelesszen_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs--primary nav nav-pills">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }

  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs--secondary pagination pagination-sm">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * @see theme_menu_local_task()
 */
function foemorelesszen_menu_local_task(&$variables) {

  $link = $variables['element']['#link'];
  $link_text = $link['title'];
  $link['localized_options']['attributes']['class'][] = 'nav-link';

  if (!empty($variables['element']['#active'])) {

    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array(
      '!local-task-title' => $link['title'],
      '!active' => $active,
    ));
    // $link_opts['attributes']['class'][] = 'active';
  }

  return '<li class="nav-item">' . l($link_text, $link['href'], $link['localized_options']) . "</li>\n";
}

function foemorelesszen_field_collection_view($variables) {
  $element = $variables['element'];
  return $element['#children'];
}

function _foemorelesszen_get_layout () {

  $layoutTest = 'default';

  if (arg(0)=='node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if (isset($node->field_layout) && isset($node->field_layout['und'][0]['value'])) {
      $layoutTest = $node->field_layout['und'][0]['value'];
    }
  }

  $params = drupal_get_query_parameters();

  if (isset($params['new']) && $params['new'] !== 0) {
    $layoutTest = 'sticky';
  } else if (isset($params['layout'])) {
    $layoutTest = $params['layout'];
  }

  switch ($layoutTest) {

    case 'sticky': case 'new':
      $layout = 'sticky';
      break;

    default:
      $layout = 'default';
  }

  return $layout;
}

include_once 'includes/preprocess/html.php';
include_once 'includes/preprocess/page.php';
include_once 'includes/preprocess/node.php';
include_once 'includes/preprocess/webform.php';
include_once 'includes/preprocess/field.php';

include_once 'includes/alter/page.php';