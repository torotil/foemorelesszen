<?php 
  $params = drupal_get_query_parameters();
  $container_class = (isset($params['new']) && $params['new'] !== 0) ? 'container-fluid' : 'container';
?>

<header class="navbar foe-header js-menustack-stuck" id="js-foe-header" role="banner">
  <div class="<?php echo $container_class; ?>">
    <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home">
      <?php if ($logo): ?>
        <img class="foe-logo" src="<?php print $logo; ?>">
      <?php else: ?>
        <img class="foe-logo" src="https://brand.friendsoftheearth.uk/themes/custom/foebrand/img/foe-logo-horizontal.svg">
      <?php endif; ?>    
    </a>

  <?php if ($main_menu): ?>
    <div class="foe-header-content d-none d-sm-block">
      <nav role="navigation" aria-labelledby="main-navigation-menu" id="main-navigation">
        <h2 class="sr-only" id="main-navigation-menu"><?php echo t('Main menu'); ?></h2>
        <?php $main_menu_output = menu_tree_output(menu_tree_all_data('main-menu')); ?>
        <?php echo drupal_render($main_menu_output); ?>
      </nav>
    </div>
  <?php endif; ?>
  <?php print render($page['header']); ?>

</div>
</header>
