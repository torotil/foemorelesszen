<?php


/* ----- Customization here ---- */

// (TODO: Make/reuse a Campaignion content type and pull these values from fields)


/* The target page and the share image */
$targetPath = '/act/hidden-plastics-quiz';
$imagePath  = '/' . path_to_theme() . '/assets/hidden-plastics-quiz/static/img/share.jpg';


/* The analytics UTM values */
$utm_source = 'facebook'; // May be overridden based on crawler agent
$utm_campaign = 'plastics';
$utm_medium = 'social';
$utm_content = '2018-07_hidden-plastics-quiz'; // Will have '_vX` appended to indicate version


/* ----- End customization ---- */



/* ----- No changes below pls thx ---- */

// Override the utm_source parameter if we can confidently map it to a referrer
if (isset($_SERVER['HTTP_REFERER'])) {

  // If it's Twitter (t.co)
  if (preg_match("/\/t\.co\//i", $_SERVER['HTTP_REFERER'])) {
    $utm_source = 'twitter';
  }

}



$host = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];
$target = $host . $targetPath;
$image  = $host . $imagePath;

/* Parameter structure: quizdata=[score]/[variant] */
$variant = 1; // Default
if (!empty($_GET['quizdata'])) {
  $dataRaw = $_GET['quizdata'];
  $data = explode('/', $dataRaw);
  if (count($data) == 2 ) {
    $score = $data[0];
    $variant = $data[1];
  }
}

$utmString = '?utm_source=' . $utm_source .
             '&utm_campaign=' . $utm_campaign .
             '&utm_medium=' . $utm_medium .
             '&utm_content=' . $utm_content . '-v' . $variant;


/* Set share default copy (== variant 1)*/
$title = 'Can you tell where the hidden plastics are in your home? Find out >>';
$description = "Hardly anyone knows about all of these – do you? Take the test.";

/* Handle different variants */
if (isset($score)) { // Will only be set if we've got quizdata
  if ($variant == 1) {
    $title = 'Can you tell where the hidden plastics are in your home? Find out >>';
    $description = "Hardly anyone knows about all of these – do you? Take the test.";
  } else if ($variant == 2) {
    $title = 'Are you using plastic without knowing? Find out now >>';
    $description = "Hardly anyone knows about all of these – do you? Take the test.";
  } else if ($variant == 3) {
    $title = 'Are you using plastic without knowing? I got '.$score.' out of 8 right >>';
    $description = "Think you can do better? Take the test.";
  }
}


// NOTE: The Location: header is now disabled -- we're adding a <meta> refresh instead,
// which Twitter/Facebook bots won't follow, meaning we can simply cache one
// version of the rendered page.

/* Redirect humans to the real page */
// if (! preg_match("/facebook|facebot|twitterbot/i", $_SERVER['HTTP_USER_AGENT'])) {
//   header("Location: " . $target . $utmString);
// }


// NOTE: Cache disabling code is now disabled -- see above.
/* Prevent caching -- stops Varnish from caching the bot-retrieved version of the page */
// header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
// header("Pragma: no-cache"); // HTTP 1.0.
// header("Expires: 0"); // Proxies.

/* Belt and braces -- set Drupal caching off, if we're running under a Drupal framework */
// if (function_exists('drupal_page_is_cacheable')) {
//   drupal_page_is_cacheable(false);
// }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="refresh" content="0; url=<?php echo $target . $utmString;?>"/>
    <meta property="og:title" content="<?php echo $title ?>"/>
    <meta property="og:description" content="<?php echo $description; ?>"/>
    <meta property="og:image" content="<?php echo $image ?>"/>
    <meta property="og:image:secure_url" content="<?php echo $image ?>"/>
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:url" content="<?php echo 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>"/>
    <meta property="og:type" content="website">
    <title></title>
  </head>
  <body onload="window.location = '<?php echo $target . $utmString;?>'">
    <p id="quiz_link">
      If this message remains for more than a few seconds, you can
      <a href="<?php echo $target . $utmString;?>">take the
      hidden plastics quiz here</a>.
    </p>
    <script>
      document.getElementById("quiz_link").style.display = 'none';
      window.setTimeout(function(){
        document.getElementById("quiz_link").style.display = 'block';
      }, 3000);
    </script>
  </body>
</html>
