<!-- BEGIN Forward Action CSS -->
  <link href="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/css/main.css" rel="stylesheet">
    <!--
      ...it appears to be okay to have a <link> in the <body>, but assume it'll sometimes cause a FOUC, so perhaps this CSS
      should be inlined. See https://html.spec.whatwg.org/multipage/links.html#link-type-stylesheet:link-type-stylesheet
    -->
<!-- END Forward Action CSS -->


<!-- BEGIN Forward Action HTML -->
  <div id="slide-container">

    <!-- Score slide -->
    <div class="slide" id="slide-score" data-slide="0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="text-title-deco">You scored X out&nbsp;of&nbsp;X</div>
                    <div class="score-container">
                        <div class="text-score-subtitle">Challenge your friends to see if they can do better - <strong>will you share the quiz now?</strong></div>
                        <div class="buttons-container">
                            <button class="btn btn-primary skip gotoshare" data-target="slide-share">Yes, of course</button>
                            <button class="btn btn-primary skip" data-target="slide-donation" style="opacity:.8;">No, sorry</button>
                        </div>
                        <button class="btn-text skip" data-target="slide-score-breakdown">See my score breakdown</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Score breakdown slide -->
    <div class="slide" id="slide-score-breakdown" data-slide="1">
        <div class="container score-breakdown-container">
            <div class="row">
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score0">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/0a.jpg" class="question-card-img" alt="Chewing gum" />
                        <div class="question-card-text">Q1. Chewing gum
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q1. Chewing gum</div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score1">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/1c.jpg" class="question-card-img" alt="Teabags" />
                        <div class="question-card-text">Q2. Teabags
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q2. Teabags</div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score2">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/2c.jpg" class="question-card-img" alt="Cartons" />
                        <div class="question-card-text">Q3. Cartons
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q3. Cartons</div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score3">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/3c.jpg" class="question-card-img" alt="Paper cup (without lid)" />
                        <div class="question-card-text">Q4. Paper cup (without lid)
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q4. Paper cup (without lid)</div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score4">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/4b.jpg" class="question-card-img" alt="Crisp packets" />
                        <div class="question-card-text">Q5. Crisp packets
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q5. Crisp packets </div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score5">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/5a.jpg" class="question-card-img" alt="Light bulbs" />
                        <div class="question-card-text">Q6. Light bulbs
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q6. Light bulbs</div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score6">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/6b.jpg" class="question-card-img" alt="Wet wipes" />
                        <div class="question-card-text">Q7. Wet wipes
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q7. Wet wipes</div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Score box -->
                <div class="col-md-4 question-card-container">
                    <div class="question-card" id="score7">
                        <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/7b.jpg" class="question-card-img" alt="Cardboard food box" />
                        <div class="question-card-text">Q8. Cardboard food box
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-info.png" class="question-card-text-img" alt="Info" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img question-card-overlay-img-mobile" alt="Close" />
                        </div>
                        <div class="question-card-icon">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check-transparent.png" class="question-card-icon-img right" alt="Correct" />
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-icon-img wrong" alt="Wrong" />
                        </div>
                        <div class="question-card-overlay">
                            <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-x.png" class="question-card-overlay-img" alt="Close" />
                            <div class="title">Q8. Cardboard food box</div>
                            <div class="copy"></div>
                        </div>
                    </div>
                </div>
                <!-- Think we got it wrong box -->
                <div class="col-md-4 question-card-container">
                    <!-- // Removed due to volume of messages :(
                    <div class="gotitwrong-card">
                        <div class="gotitwrong-title">Think we’ve got it wrong?</div>
                        <div>Plastic is now used in so many items that it’s often hard to be 100% sure things don’t contain plastic – even for us! If you’d like to let us know there’s plastic in something that we’ve said is plastic free, <a href="https://friendsoftheearth.uk/contact?enquiry_type=general&enquiry_detail_general=other&message=Hidden+plastics+quiz+feedback.%0A%0A#block-email-us-form" target="_blank">send us a message</a>.</div>
                    </div>
                    -->
                </div>
            </div>
            <div class="text-center">
              <button class="btn-text skip" data-target="slide-share">Challenge your friends?</button>
            </div>
        </div>
    </div>

    <!-- Share slide -->
    <div class="slide" id="slide-share" data-slide="2">
        <div class="container">
            <div class="row row-main">
                <div class="col-md-12">
                    <div class="share-container">
                        <div class="text-title text-center mb20">Great, thanks – just click below to share:</div>
                        <div class="share-buttons">
                            <button class="skip btn btn-default btn-facebook"><img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/fb.png" alt="Facebook" /> Share on Facebook</button>
                            <button class="skip btn btn-default btn-twitter"><img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/twitter.png" alt="Twitter" /> Share on Twitter</button>
                            <button class="skip btn btn-default btn-email"><img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/email.png" alt="Email" /> Share by Email</button>
                            <button class="skip btn btn-default btn-whatsapp"><img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/whatsapp.png" alt="WhatsApp" /> Share on WhatsApp</button>
                        </div>
                        <div class="share-preview-container">
                            <div class="share-preview-img">
                                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/share.jpg" id="share-img" alt="Plastics" />
                            </div>
                            <div class="share-preview-text">
                                <div class="share-preview-title">Can you tell where the hidden plastics are in your home? Find out >></div>
                                <div class="share-preview-description">Hardly anyone knows about all of these – do you? Take the test.</div>
                            </div>
                        </div>
                        <div class="text-center btn-beforego">
                            <button class="btn btn-primary skip" data-target="slide-donation">Before you go</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Donation slide -->
    <div class="slide" id="slide-donation" data-slide="3">
        <div class="container before-results-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="text-title text-white mb20">Will you donate to help stop plastic pollution?</div>
                    <div class="text-default text-white mb20 desktop-only">
                        <p>Together, we can stop the tsunami of plastic pouring into our oceans.</p>
                        <p>But to keep up the pressure on the government to take action, we need to grow our campaign.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-white card-donate">
                        <div class="text-cta text-center mb20">Donate to Friends of the Earth:</div>
                        <div class="buttons-container buttons-container-donate">
                            <a class="btn btn-primary" href="https://act.friendsoftheearth.uk/donate/donate-end-plastic-pollution?source=FN190025W#p:donation_interval=1&select=15"><strong>£15</strong></a>
                            <a class="btn btn-primary" href="https://act.friendsoftheearth.uk/donate/donate-end-plastic-pollution?source=FN190025W#p:donation_interval=1&select=25"><strong>£25</strong></a>
                            <a class="btn btn-primary" href="https://act.friendsoftheearth.uk/donate/donate-end-plastic-pollution?source=FN190025W#p:donation_interval=1&select=50"><strong>£50</strong></a>
                            <a class="btn btn-primary" href="https://act.friendsoftheearth.uk/donate/donate-end-plastic-pollution?source=FN190025W#p:donation_interval=1&select=select_or_other&other="><strong>Other</strong></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mobile-only">
                    <div class="text-default text-white mb20">
                        <p>Together, we can stop to the tsunami of plastic pouring into our oceans.</p>
                        <p>But to keep up the pressure on the government to take action, we need to grow our campaign.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- END Forward Action HTML -->



<!-- BEGIN Forward Action Javascript -->
  <script type="text/javascript" src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/js/abtester.min.js"></script>
  <script type="text/javascript" src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/js/tests.js"></script>
  <script type="text/javascript" src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/js/main.js"></script>
<!-- END Forward Action Javascript -->
