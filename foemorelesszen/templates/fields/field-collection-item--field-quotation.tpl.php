<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

if(isset($content['field_image'])) {
  $style_name = 'background_image';
  $image_uri = $content['field_image'][0]['#item']['uri'];
  $derivative_uri = image_style_path($style_name, $image_uri);
  $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style_name), $image_uri, $derivative_uri);
  $new_image_url = file_create_url($derivative_uri);

  $image_attributes = "style=\"background-image:url($new_image_url);\"";
  hide($content['field_image']);
}
?>
            

<div class="cck-quotation" <?php print $attributes; ?>>
  <div class="background-image" <?php print $image_attributes; ?>></div>
  <div class="container">
    <div class="row">
      <div class="quotation-box col-12 col-sm-10 push-sm-1 col-md-6 push-md-5"<?php print $content_attributes; ?>>
        <div class="quotation-box-inner">
          <?php hide($content['field_text']); ?>
          <?php hide($content['field_small_text']); ?>
          <div class="quote-text">
            <?php print render($content['field_text']); ?>
          </div>
          <div class="quote-author">
            &mdash; <?php print render($content['field_small_text']); ?>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>
