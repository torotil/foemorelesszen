<svg class="loading-icon" xmlns="http://www.w3.org/2000/svg" viewBox="-100 -100 200 200">
  <clipPath id="loading-icon-clip-path">
    <circle r="95"></circle>
  </clipPath>
  
  <circle class="loading-icon__background" r="95" fill="#f3f3f7"></circle>
  <circle class="loading-icon__animated-stroke" clip-path="url(#loading-icon-clip-path)" r="100" fill="none" stroke-width="20px" stroke="#aaaaaa"></circle>
  <circle class="loading-icon__static-stroke-section" clip-path="url(#loading-icon-clip-path)" r="100" fill="none" stroke-width="20px" stroke="#aaaaaa"></circle>
  <image xlink:href="https://image.flaticon.com/icons/svg/46/46564.svg" x="-50" y="-50" width="100" height="100" opacity=".5"></image>
</svg>