<section class="foe-donation-main-2017">
  <div class="bg-image-overlay"></div>
  <div class="container">

    <header class="article-header row">
      <div class="col-12">

        <?php print render($title_prefix); ?>
        <h1>
          <span class="display-1" <?php print $title_attributes; ?>>
          <?php if ( field_get_items('node', $node, 'field_public_title') ): 
            print render($content['field_public_title']); 
          else: 
            print $title; 
          endif ?>
          </span>
          <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
            <span class="display-2" <?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></span>
          <?php endif ?>
        </h1>
        <?php print render($title_suffix); ?>

      </div>
    </header>

    <?php if ($display_submitted): ?>
      <div class="submitted">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>
    <div class="row">
      <div class="donation-box col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-xl-4 offset-xl-4"<?php print $content_attributes; ?>>
        <div class="donation-box-inner">
          <?php
            hide($content['field_sticker']);
            hide($content['comments']);
            hide($content['links']);
            hide($content['body']);
            // print render($content);
            print render($content['pgbar_default']);
            print render($content['webform']);
          ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="donation-certifications col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-xl-4 offset-xl-4">
        <div class="logos"></div>
      </div>
    </div>

  </div>

  <?php if ( $content['field_sticker']): ?>
    <div class="donation-sticker">
      <?php print render($content['field_sticker']); ?>
    </div>
  <?php endif; ?>

  <a data-scroll-to="#content_bottom_anchor" class="content-arrow-slide arrow_slide">Slide down</a>
</section>
