<section class="foe-new-ux-main-2018">
  <div class="new-ux-sticky-container">

    <?php
      /**
       * @todo delete this if we push this live after optimize tests
       */
      $node_alias = array_reverse(explode('/', drupal_get_path_alias()))[0];
    ?>
    <header class="article-header <?php echo $node_alias; ?>" style="background-image:url(<?php echo $main_image_url; ?>);">
      <div class="col-12 col-sm-10 offset-sm-1">
        <?php print render($title_prefix); ?>
        <h1>
          <span class="display-1" <?php print $title_attributes; ?>><?php print $title; ?></span>
          <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
            <span class="display-2" <?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></span>
          <?php endif ?>
        </h1>
        <?php print render($title_suffix); ?>
      </div>
    </header>

    <section class="article-body"<?php print $content_attributes; ?>>
      <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
        <div class="field-body donation-copy">
          <?php
            hide($content['field_sticker']);
            hide($content['comments']);
            hide($content['links']);
            hide($content['webform']);
            print render($content);
          ?>
        </div>
      </div>
    </section>

  </div>

  <aside class="new-ux-sticky">
    <div class="donation-sidebar">
      <div class="prefill-salutation element-invisible">
        <h3><span class="js-prefill-name"></span>, donate to the cause</h3>
        <a href="#" class="js-prefill-reset">not you?</a>
      </div>
      <?php
        print render($content['webform']);
      ?>
    </div>
  </aside>

  <?php // This thingie (text and visibility) is driven from JS ?>
  <div class="new-ux-fixed-button text-center">
    <button class="d-block d-md-inline-block" data-scroll-to=".new-ux-sticky"></button>
  </div>
</section>
