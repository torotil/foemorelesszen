<?php $classes .= ' node-petition-sidebar'; ?>
<article data-nid="<?php print $node->nid; ?>" id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/petition/'._foemorelesszen_get_layout().'.tpl.php'); ?>

</article>
