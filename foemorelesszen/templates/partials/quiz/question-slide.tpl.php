<section class="quiz-slide quiz-slide-question foe-quiz-slide d-none" data-question-id="<?php echo $id; ?>" >  
  <div class="container">
    <div class="row justify-content-center mt-4">
      <div class="<?php echo $quiz_classes; ?>">
        <h4><?php echo $slide->question->text; ?></h4>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="<?php echo $quiz_classes; ?>">
        <figure class="question-img my-4">
          <img class="img-fluid" src="about:blank" />
        </figure>
      </div>
    </div>
    
    <div class="row justify-content-center mb-4">
      <div class="<?php echo $quiz_classes; ?>">
        <div class="row">
          <?php foreach ($slide->answers as $answerId => $answer): ?> 
          <div class="col-6">
            <button foe-quiz-answer="<?php echo $answerId; ?>" class="btn btn-outline-offwhite btn-rounded btn-block"><?php echo $answer->text; ?></button>
          </div>
          <?php endforeach; ?> 
        </div> 
      </div> 
    </div> 
  </div> 
</section>