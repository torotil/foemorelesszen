<?php 
  // Load our quiz
  $params = drupal_get_query_parameters();
  $node_quiz = node_load($params['quiz']);
  $quiz_json = field_get_items('node', $node_quiz, 'quiz_config')[0]['value'];

  $quiz_slides = json_decode($quiz_json);
  $quiz_slides_total = count($quiz_slides);
  
  $quiz_cookie = array();

  $score = 0;

  $field_background_image = field_get_items('node', $node, 'field_main_image');
  
  if ($field_background_image) {
    $style_name = 'background_image';
    $image_uri = $field_background_image[0]['uri'];
    $derivative_uri = image_style_path($style_name, $image_uri);
    $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style_name), $image_uri, $derivative_uri);
    $main_image_url  = file_create_url($derivative_uri);
  }

?>


<article foe-quiz data-quiz="<?php print $params['quiz']; ?>" data-nid="<?php print $node->nid; ?>" id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- Score slide -->
  <section id="score" class="fs-slide d-flex align-items-center bg-extradark">
    <div class="foe-media-holder jarallax">
      <i class="jarallax-img foe-quiz-bg" style="background-image:url(<?php echo $main_image_url; ?>)"></i> 
    </div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">

          <div class="row my-4">
            <div class="col-12 text-center">
              <h3 class="my-4"><?php print t('You scored'); ?> <span class="js-score-result"><i class="fas fa-spinner fa-spin"></i></span> <?php print t('out of @total', array('@total' => $quiz_slides_total)); ?></h3>
              <h3 class="my-4"><?php print t('Challenge your friends to see if they can do better'); ?> &mdash; 
                <strong><?php print t('will you share the quiz now?'); ?></strong></h3>
            </div>
          </div>

          <div class="row my-4">
            <div class="col-12 col-lg-6 mb-4">
                <button class="btn btn-rounded btn-accent btn-block" foe-thanks-reveal="share"><?php print t('Yes, of course'); ?></button>
            </div>
            <div class="col-12 col-lg-6 mb-4">
                <button class="btn btn-rounded btn-outline-offwhite btn-block" foe-thanks-reveal="donation"><?php print t('No, sorry'); ?></button>
            </div>
          </div>

          <div class="row my-4">
            <div class="col-12 text-center">
              <span foe-thanks-reveal="breakdown share" class="skip-breakdown"><?php print t('See my score breakdown'); ?></span>
            </div>
          </div>

        </div> 
      </div>
    </div>
  </section>

  <!-- Score breakdown slide -->
  <section id="breakdown" class="fs-slide d-none align-items-center bg-extradark">
    <div class="container">
      <div class="row mb-4 pb-4">
        <div class="col-12 text-center">
          <h4><?php print t('You scored'); ?> <span class="js-score-result"><i class="fas fa-spinner fa-spin"></i></span> <?php print t('out of @total', array('@total' => $quiz_slides_total)); ?></h4>
        </div>
      </div>
    
      <div class="row">
        <?php foreach ($quiz_slides as $id => $slide): ?>

          <div class="col-12 col-md-6 col-xl-4 score-card-container">
            <h5><?php echo 'Q', $id+1, '. ', $slide->question->text; ?></h5>  
            
            <div class="text-offwhite mb-2">
                <h6 class="py-2 text-center bg-offwhite text-extradark"><?php print t('You said: '); ?><span class="js-score-card-answer-user"></span></h6>                
                <h4 class="py-2 text-center bg-success js-score-card-correct">
                  <i class="far fa-check-circle"></i> <?php print t('Correct, it\'s @answer!', array('@answer' => lcfirst($slide->answers[ $slide->correctAnswer ]->text))); ?>
                </h4>
                <h4 class="py-2 text-center bg-danger js-score-card-wrong">
                  <i class="far fa-times-circle"></i> <?php print t('Wrong, it\'s @answer.', array('@answer' => lcfirst($slide->answers[ $slide->correctAnswer ]->text))); ?>
                </h4>
            </div>  
            <div class="score-card" style="background-image: url(<?php echo $slide->question->image; ?>)">
                <div class="score-card-overlay">
                  <div class="score-copy">
                    <p class="solution"><?php echo nl2br($slide->solutionText); ?></p>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="text-center">
            <?php $feedback_link = 'https://friendsoftheearth.uk/contact?enquiry_type=general&enquiry_detail_general=other&message=I+have+some+feedback+for+the+quiz+'.urlencode($node_quiz->title).'.%0A%0A#block-email-us-form'; ?>
            <h5><?php print t('Think we’ve got it wrong?'); ?></h5>
            <p><?php print t('We don\'t always get things right. If you have any queries about the answers please ');
                     print l('send us a message', $feedback_link); ?></p>
          </div>
        </div>
      </div>

    </div>
  </section>

<?php

  // Set up defaults
  $share['target'] = url('/'
    . drupal_get_path_alias('node/' . $_GET['quiz']), array('absolute' => TRUE))
    . '?share_data='; // Empty, gets a placeholder in a sec, then populated client-side by JS
  $share['target_encoded'] = urlencode($share['target']);
  
  /**
   * TODO: Put these placeholders into JS-accessible variables instead of
   * hardcoding them both here and in quizthanks.service.js
   */
  $share['target'] .= '__share_data__'; // Ew
  $share['target_encoded'] .= '__share_data_encoded__'; // Again, ew.

  $share['image'] = 'https://placehold.it/1200x628';
  $share['title'] = 'WARNING: Couldn\'t load title';
  $share['description'] = 'WARNING: Couldn\'t load the page\'s description.';


  if (isset($node_quiz)) {

    // Default share copy
    $custom_share_title = 'I got !score out of !questions right. Can you do better?';
    $custom_share_description = $node_quiz->title;

    // Custom share copy, if available
    if (isset($node_quiz->field_custom_share_title) && !empty($node_quiz->field_custom_share_title)) {
      $custom_share_title = $node_quiz->field_custom_share_title['und'][0]['safe_value'];
    }
    if (isset($node_quiz->field_custom_share_description) && !empty($node_quiz->field_custom_share_description)) {
      $custom_share_description = $node_quiz->field_custom_share_description['und'][0]['safe_value'];
    }

    $share['title'] = str_replace(
      ['!score', '!questions'],
      ['<span class="js-score-result"><i class="fas fa-spinner fa-spin"></i></span>', $quiz_slides_total],
      $custom_share_title
    );

    $share['description'] = str_replace(
      ['!score', '!questions'],
      ['<span class="js-score-result"><i class="fas fa-spinner fa-spin"></i></span>', $quiz_slides_total],
      $custom_share_description
    );

    $share['image'] = file_create_url(field_get_items('node', $node_quiz, 'opengraph_meta_image')[0]['uri']);

  }

?>
  
  <?php $share_light = $node->share_light['und'][0]['options']; ?>
  <!-- Share slide -->
  <section id="share" class="fs-slide d-none align-items-center bg-extradark">
    <div class="container">

    <div class="row mb-4">
      <div class="col-12 text-center">
        <h3><?php print $share_light['subject']; ?></h3>
      </div>
    </div>
    
      <?php
        $share_channels = $share_light['channels'];

        $shares = array(
          'facebook' => array(
            'name' => 'Facebook',
            'icon' => 'fab fa-facebook-f',
            'base_url' => 'https://www.facebook.com/sharer/sharer.php?u=',
            'url' => $share['target_encoded'],
            'mobile_only' => false,
          ),
          'twitter' => array(
            'name' => 'Twitter',
            'icon' => 'fab fa-twitter',
            'base_url' => 'https://twitter.com/intent/tweet?text=',
            'url' => rawurlencode($share_channels['twitter']['text']) . '%20' . $share['target_encoded'],
            'mobile_only' => false,
          ),
          'whatsapp' => array(
            'name' => 'Whatsapp',
            'icon' => 'fab fa-whatsapp',
            'base_url' => 'whatsapp://send?text=',
            'url' => rawurlencode($share_channels['whatsapp']['text']) . '%20' . $share['target_encoded'],
            'mobile_only' => true,
          ),
          'fbmsg' => array(
            'name' => 'Messenger',
            'icon' => 'fab fa-facebook-messenger',
            'base_url' => 'fb-messenger://share/?link=',
            'url' => $share['target_encoded'],
            'mobile_only' => true,
          ),
          'email' => array(
            'name' => 'E-mail',
            'icon' => 'far fa-envelope',
            'base_url' => 'mailto:?subject=' . rawurlencode($share_channels['email']['mailto']['subject']),
            'url' => '&body=' . rawurlencode($share_channels['email']['mailto']['body'] . "\n\n" . $share['target']),
            'mobile_only' => false,
          ),
        );      
      ?>
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
          <div class="row justify-content-center">
            <?php foreach($shares as $channel => $values): 
              if ($share_channels[$channel]['toggle'] == 1): ?>

                <div class="col-12 mb-4 <?php echo ($values['mobile_only']) ? 'd-md-none' : 'col-md-6'; ?>">
                  <a href="<?php print $values['base_url'] . $values['url']; ?>" target="_blank" rel="nofollow" class="btn btn-outline-offwhite btn-rounded btn-block" data-click-track="{&quot;eventCategory&quot;:&quot;share&quot;,&quot;eventAction&quot;:&quot;quiz-results-<?php print $channel ?>-share&quot;}">
                    <i class="<?php print $values['icon']; ?>"></i> 
                    <?php print t('Share on @platform', array('@platform' => $values['name'])); ?>
                  </a>
                </div>

              <?php endif; ?>
            <?php endforeach; ?>
          </div>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
          <section class="foe-quiz-share-preview">
            <figure class="foe-quiz-share-image">
              <img src="<?php print $share['image']; ?>" />
              <div class="foe-quiz-share-data">
                <h4><?php print $share['title']; ?></h4>
                <p><?php print $share['description']; ?></p>
              </div> 
            </figure>
          </section>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-12 col-lg-8 col-xl-6">
          <button class="btn btn-rounded btn-accent btn-block" foe-thanks-reveal="donation"><?php print t('Before you go'); ?></a>
        </div>
      </div>

    </div>
  </section>

  <!-- Donation slide -->
  <section id="donation" class="fs-slide d-none align-items-center bg-offwhite">
    <div class="foe-media-holder jarallax">
      <i class="jarallax-img foe-quiz-bg" style="background-image:url(<?php echo $main_image_url; ?>)"></i> 
    </div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10">
          <div class="foe-thanks-action-slide bg-extradark">
            <div class="row align-items-center">
              
              <div class="col-12 col-lg-6">
                <?php print render($content['field_action_html']);?>
              </div>

              <div class="col-12 col-lg-6">
                <?php if(!empty(field_get_items('node', $node, 'field_action_slide_embedded_form')[0]['nid'])): ?>
                  <?php $node_embed = node_load(field_get_items('node', $node, 'field_action_slide_embedded_form')[0]['nid']); ?>
                  <div class="quiz-override is-double node-<?php print $node_embed->type; ?>">
                  <?php
                    webform_node_view($node_embed, 'full');
                    print theme_webform_view($node_embed->content);
                  ?>
                  </div>
                <?php endif; ?>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Score breakdown slide -->
  <section id="challenge-cta" class="position-fixed fixed-bottom">
    <button class="btn btn-extradark btn-rounded" foe-thanks-reveal="share"><?php print t('Challenge your friends'); ?></button>
  </section>

</article>
