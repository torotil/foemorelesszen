<?php 
/*
 * @file action-slide.tpl.php
 * @project foemorelesszen
 * 
 * @author Luigi Mannoni (hello@luigimannoni.com)
 * @created Thursday, 5th July 2018 4:23:10 pm
 * 
 * @last-modified Thursday, 5th July 2018 4:23:24 pm
 *                Luigi Mannoni (hello@luigimannoni.com)
 */
?>
<section class="foe-scrolling-thank-you-page-section" id="slide-section-action">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="foe-scrolling-thank-you-page-section__inner">
          <?php 
            hide($content['field_action_slide_embedded_form']);
            hide($content['field_action_html']);
          ?>
          
          <?php if(!empty($content['field_action_html'])): ?>
            <?php if(field_get_items('node', $node, 'field_action_html_boxed')[0]['value'] === '0'): ?>
            <header class="row article-header text-center">
              <div class="col-12">
                <h3><?php print render($content['field_action_html']);?></h3>
              </div>
            </header>
            <?php else: ?>
            <div class="foe-box-solid">
              <?php print render($content['field_action_html']);?>
            </div>
            <?php endif; ?>
          <?php endif; ?>

          <?php if(!empty(field_get_items('node', $node, 'field_action_slide_embedded_form')[0]['nid'])): ?>
            <?php $node_embed = node_load(field_get_items('node', $node, 'field_action_slide_embedded_form')[0]['nid']); ?>
            <div class="foe-box-solid node-<?php print $node_embed->type; ?>">
              <?php
                webform_node_view($node_embed, 'full');
                print theme_webform_view($node_embed->content);
              ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
  