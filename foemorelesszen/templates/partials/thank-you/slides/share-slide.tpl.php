<?php
/*
 * @file share-slide.tpl.php
 * @project foemorelesszen
 *
 * @author Luigi Mannoni (hello@luigimannoni.com)
 * @created Thursday, 5th July 2018 3:12:21 pm
 *
 * @last-modified Thursday, 6th February 2019 11:33:42 am
 *                James South (j@jrsouth.com)
 */

if ($node->share_light['und'][0]['toggle'] == '1'): ?>

  <section class="foe-scrolling-thank-you-page-section" id="slide-section-share">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12 col-sm-8 col-md-6">
          <div class="foe-scrolling-thank-you-page-section__inner">
            <div class="foe-sharer-preview">
              <div class="foe-sharer-profile">
                <div class="user-profile-image"></div>
                <div class="user-profile-metadata">
                  <span class="name">Friends of the Earth</span>
                  <span class="date">Just now<span role="presentation" aria-hidden="true"> · </span><i class="fas fa-globe-africa"></i></span>
                </div>
              </div>
              <div class="foe-sharer-image" style="background-image:url('<?php echo $share['image']; ?>')"></div>
              <div class="foe-sharer-post-data">
                <p class="foe-sharer-title"><?php echo $share['title']; ?></p>
                <p class="foe-sharer-description"><?php echo $share['description']; ?></p>
              </div>

              <?php foreach($shares as $channel => $values):
                if (isset($share_channels[$channel]) && $share_channels[$channel]['toggle'] == 1): ?>
                    <a href="<?php print $values['base_url'] . $values['url']; ?>" target="_blank" rel="nofollow" class="mb-2 btn btn-block btn-<?php echo strtolower($values['name']); echo ($values['mobile_only']) ? ' d-md-none' : '';?>" data-click-track="{&quot;eventCategory&quot;:&quot;share&quot;,&quot;eventAction&quot;:&quot;quiz-results-<?php print $channel ?>-share&quot;}">
                      <i class="<?php print $values['icon']; ?>"></i>
                      <?php print t('Share on @platform', array('@platform' => $values['name'])); ?>
                    </a>
                <?php endif; ?>
              <?php endforeach; ?>

              <a class="btn btn-offwhite btn-sm btn-block" href="#slide-section-action" data-click-track="{&quot;eventCategory&quot;:&quot;interaction&quot;,&quot;eventAction&quot;:&quot;scrolling_thank_you_skip&quot;}">Skip<span class="d-none d-sm-inline"> sharing</span></a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php endif; ?>
