<?php

/**
 * NOTE: We're not using a Location: header because of issues with
 * caching -- we're using a <meta> refresh instead, plus a JS function,
 * and a fallback manual hyperlink that becomes visible after a few
 * seconds, all of which the Twitter/Facebook bots will ignore.
 * 
 */ 


// Set URLs
$target = $share_data['target_url'];
$image  = $share_data['image_url'];

// Set share copy
$title = $share_data['title'];
$description = $share_data['description'];

/* Analytics UTM values */
$utm_medium = 'social';
$utm_source = 'unspecified'; // Default, should be overridden based on referral info
$utm_campaign = $share_data['campaign'];
$utm_content = strftime('%Y-%m') . '_quiz-share-' . drupal_html_class($share_data['target_title']);


// Override the utm_source parameter if we can confidently map it to a known referrer
if (isset($_SERVER['HTTP_REFERER'])) {
  
  // If it's Twitter
  if (preg_match("/\/t\.co\//i", $_SERVER['HTTP_REFERER'])) {
    $utm_source = 'twitter';
  }
  
  // If it's Facebook
  if (preg_match("/\/facebook\.com\//i", $_SERVER['HTTP_REFERER'])) {
    $utm_source = 'facebook';
  }
  
}

$utmString = '?utm_source=' . $utm_source .
             '&utm_campaign=' . $utm_campaign .
             '&utm_medium=' . $utm_medium .
             '&utm_content=' . $utm_content;

?>


<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="refresh" content="0; url=<?php echo $target . $utmString;?>"/>
    <meta property="og:title" content="<?php echo $title ?>"/>
    <meta property="og:description" content="<?php echo $description; ?>"/>
    <meta property="og:image" content="<?php echo $image ?>"/>
    <meta property="og:image:secure_url" content="<?php echo $image ?>"/>
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="<?php echo $share_data['image_width']; ?>" />
    <meta property="og:image:height" content="<?php echo $share_data['image_height']; ?>" />
    <meta property="og:url" content="<?php echo 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . 
      '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>"/>
    <meta property="og:type" content="website">
    <title><?php echo $share_data['target_title']; ?></title>
  </head>
  <body onload="window.location = '<?php echo $target . $utmString;?>'">
    <p id="quiz_link">
      If this message remains for more than a few seconds, you can
      <a href="<?php echo $target . $utmString;?>">click here</a>.
    </p>
    <script>
      document.getElementById("quiz_link").style.display = 'none';
      window.setTimeout(function(){
        document.getElementById("quiz_link").style.display = 'block';
      }, 3000);
    </script>
  </body>
</html>