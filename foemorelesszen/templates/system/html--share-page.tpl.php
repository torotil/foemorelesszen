<?php

/* Template to render a share-page node with custom metadata, enabling
 * gamification.
 *
 * Social platforms collect and display the custom meta data, while real
 * visitors are directed to the intended target, with some sensible analytics
 * tracking parameters injected.
 *
 * (Currently) relies on valid JSON being available to represent both the
 * variants, and the available options.
 *
 * Query string parameter "v" indicates the variant to use (treated as a
 * one-indexed array). This is mapped against an array of associative arrays,
 * provided by field_variant_json.
 *
 * Query string parameter "d" indicates the data that will be used to customise the
 * share, with array keys separated by "/", e.g. ?d=2/cat/5
 *
 * These keys are sequentially mapped against an array of associative arrays,
 * provided by field_option_json.
 *
 * In the absence or invalidity of any parameter the first of the options
 * available will be used.
 *
 */

if(arg(0) == 'node' && !empty(arg(1))) {
  $node = node_load(arg(1));
}


// Let's do the JSON decoding early -- we want to fail quickly if the data supplied
// via Drupal fields isn't valid. (And we want to exit with a succinct message.)
$variant_json = field_get_items('node', $node, 'field_variant_json')[0]['value'];
$option_json = field_get_items('node', $node, 'field_option_json')[0]['value'];

$variants = json_decode($variant_json);
if ($variants === null && json_last_error() !== JSON_ERROR_NONE) {
  http_response_code(500);
  echo "<strong>Error found in the \"Variant\" JSON data.</strong>";
  echo "<br><br><pre>" . json_last_error_msg() . "</pre>";
  die();
}

$options = json_decode($option_json);
if ($options === null && json_last_error() !== JSON_ERROR_NONE) {
  http_response_code(500);
  echo "<strong>Error found in the \"Option\" JSON data.</strong>";
  echo "<br><br><pre>" . json_last_error_msg() . "</pre>";
  die();
}
// TODO: More checking of the JSON structure to aid debugging, e.g. do we have
//       at least one variant? Do all variants define title, description, and
//       image values?


// Collect query string parameters
$params = drupal_get_query_parameters();
/* Treat as a one-indexed array because it's somewhat author facing :|  */
$current_variant = isset($params['v']) ? intval($params['v'], 10) : 1;
if ($current_variant < 1 || $current_variant > count($variants) ) {
  $current_variant = 1;
}
/* Parameter structure: d=key1/key2/key3 etc. -- converted to an array */
$current_data = isset($params['d']) ? explode('/', $params['d']) : [];


// Get the target page that's being shared
$target = field_get_items('node', $node, 'field_link')[0]['url'];


// Set up analytics/UTM values
$utm_source = 'facebook'; // May be overridden based on crawler agent
$utm_campaign = drupal_html_class(node_load(field_get_items('node', $node, 'field_reference_to_campaign')[0]['nid'])->title);
$utm_medium = 'social';
$utm_content = date('Y-m') . '_custom-share-node-' . $node->nid . '-v' . $current_variant;
// Override the utm_source parameter if we can confidently map it to a referrer
if (isset($_SERVER['HTTP_REFERER'])) {
  // If it's Twitter (t.co)
  if (preg_match("/\/t\.co\//i", $_SERVER['HTTP_REFERER'])) {
    $utm_source = 'twitter';
  }
}
// Create the string
$utmString = '?utm_source=' . $utm_source .
'&utm_campaign=' . $utm_campaign .
'&utm_medium=' . $utm_medium .
'&utm_content=' . $utm_content;


// Handle the different variants
$variant = $variants[$current_variant - 1];
$title = $variant->title;
$description = $variant->description;
$image = $variant->image;


// Handle different options (via [token] replacement)
$data_index = 0;
foreach (get_object_vars($options) as $token => $values) {
  if (isset($current_data[$data_index]) && property_exists($values, $current_data[$data_index])) {
    $replacement = $values->{"$current_data[$data_index]"};
  } else {
    $replacement = reset($values); // Default to the first defined option
  }
  $data_index++;
  $title = str_replace('[' . $token . ']', $replacement, $title);
  $description = str_replace('[' . $token . ']', $replacement, $description);
  $image = str_replace('[' . $token . ']', $replacement, $image);
}

?>



<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="refresh" content="0; url=<?php echo $target . $utmString;?>"/>
    <meta property="og:title" content="<?php echo $title ?>"/>
    <meta property="og:description" content="<?php echo $description; ?>"/>
    <meta property="og:image" content="<?php echo $image ?>"/>
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:url" content="<?php echo 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>"/>
    <meta property="og:type" content="website">
    <title></title>
  </head>
  <body onload="window.location = '<?php echo $target . $utmString;?>'">
    <p id="target_link">
      If this message remains for more than a few seconds, you can
      <a href="<?php echo $target . $utmString;?>">manually navigate to the target page</a>.
    </p>
    <script>
      document.getElementById("target_link").style.display = 'none';
      window.setTimeout(function(){
        document.getElementById("target_link").style.display = 'block';
      }, 3000);
    </script>
  </body>
</html>
