<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
$html_attributes = "lang=\"{$language->language}\" dir=\"{$language->dir}\" {$rdf->version}{$rdf->namespaces}";
?>
<?php print $doctype; ?>
<!--[if IE 6 ]><html <?php print $html_attributes; ?> class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html <?php print $html_attributes; ?> class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html <?php print $html_attributes; ?> class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html <?php print $html_attributes; ?> class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php print $html_attributes; ?> class="no-js"><!--<![endif]-->
<head<?php print $rdf->profile; ?>>

  <?php // Skip the page-hiding snippet if we've set the appropriate Drupal variable
  if (!variable_get('foe_skip_page_hiding_snippet')) : ?>
    <!-- Start Google Optimize page-hiding snippet -->
      <style>.async-hide{opacity:0!important}</style>
      <script>
        (function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
          h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
          (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,{'GTM-KR8KNTJ':true});
      </script>
    <!-- End Google Optimize page-hiding snippet -->
  <?php endif; ?>

  <?php // Skip loading OmniConvert code if we're not on production (i.e. a non-Amazee host)
  if (!variable_get('foe_skip_omniconvert')) : ?>
    <!-- Start Omniconvert.com code -->
      <link rel="dns-prefetch" href="//app.omniconvert.com" />
      <script type="text/javascript">window._mktz=window._mktz||[];</script>
      <script src="//cdn.omniconvert.com/js/d56ae3c.js"></script>
    <!-- End Omniconvert.com code -->
  <?php endif; ?>

  <meta name="google-site-verification" content="j6eOPbDGHdoPMRechLqgoSR9L4UA7y_8mmgm-Jjmpa8" />

  <?php print $head; ?>

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame  -->
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

  <!--  Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Prevent blocking -->
  <!--[if IE 6]><![endif]-->

  <title><?php print $head_title; ?></title>

  <?php print $styles; ?>

  <?php print $scripts; ?>

  <!-- PostcodeAnywhere PCA -->
  <script>
  (function(n,t,i,r){var u,f;n[i]=n[i]||{},n[i].initial={accountCode:"FRIEN11127",host:"FRIEN11127.pcapredict.com"},n[i].on=n[i].on||function(){(n[i].onq=n[i].onq||[]).push(arguments)},u=t.createElement("script"),u.async=!0,u.src=r,f=t.getElementsByTagName("script")[0],f.parentNode.insertBefore(u,f)})(window,document,"pca","//FRIEN11127.pcapredict.com/js/sensor.js");
  
  (function ($) {
    // c.f. es5/pca_trigger.js
    if (window.pca && window.pca.on) {
      window.pca.on('load', function(_type, _id, control) {
        window.pca.currentControl = control;
        control.listen('populate', function(_address) {
          $('[name="submitted[postcode]"]').change();
          $('[name="submitted[address_line_1]"]').change();
          $('[name="submitted[address_line_2]"]').change();
          $('[name="submitted[city]"]').change();
        });
      });
    }
  })(jQuery);

  </script>
  <!-- End PostcodeAnywhere PCA -->


</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>

<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>

<!--[if lt IE 7 ]>
<script src="<?php print $morelesszen_path;?>/js/libs/dd_belatedpng.min.js"></script>
<script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-images </script>
<![endif]-->

<?php if (morelesszen_ga_enabled()): ?>
  <!-- Google Analytics : mathiasbynens.be/notes/async-analytics-snippet -->
  <script type="text/javascript">
    <!--//--><![CDATA[//><!--
    var _gaq=[['_setAccount','<?php print theme_get_setting('morelesszen_ga_trackingcode');?>'],['_trackPageview']];
    <?php if (theme_get_setting('morelesszen_ga_anonimize')): ?>
    _gaq.push (['_gat._anonymizeIp']);
    <?php endif; ?>
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
    //--><!]]>
  </script>
<?php endif; ?>

<!-- Start of moreonion Zendesk Widget script -->
<?php if(user_is_logged_in()): ?>
  <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","moreonion.zendesk.com");/*]]>*/
  </script>
<?php endif; ?>
<!-- End of moreonion Zendesk Widget script -->

</body>
</html>
