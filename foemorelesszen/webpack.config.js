// const webpack = require('webpack');
const path = require('path');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

/*
 * We've enabled UglifyJSPlugin for you! This minifies your app
 * in order to load faster and run less javascript.
 *
 * https://github.com/webpack-contrib/uglifyjs-webpack-plugin
 *
 */

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtraWatchWebpackPlugin = require('extra-watch-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = (env, argv) => {
  const isProduction = argv.mode === 'production' || process.env.NODE_ENV === 'production';
  const plugins = [];

  // Define our dev/prod plugins
  // Ugly, yeah, but we need to load them in order for different envs.
  // (or maybe consider having 2 different config files)

  if (!isProduction) {
    plugins.push(new StyleLintPlugin({
      configFile: '.stylelintrc',
      context: './src/',
      syntax: 'scss',
    }));
  }
  if (isProduction) {
    plugins.push(new CleanWebpackPlugin(['./css', './js']));
  }

  plugins.push(new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: isProduction ? 'css/style.css' : 'css/style.css',
    chunkFilename: isProduction ? 'css/[id].css' : 'css/[id].css',
  }));

  if (!isProduction) {
    plugins.push(new ExtraWatchWebpackPlugin({
      files: ['./src/**'],
    }));
  }

  if (!isProduction) {
    plugins.push(new LiveReloadPlugin({
      protocol: 'http',
      hostname: 'foe-campaignion.docker.amazee.io',
      port: 9001,
    }));
  }

  return {
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: [
            /\/core-js/,
          ],
          include: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules'),
          ],
          use: [
            {
              loader: 'babel-loader?cacheDirectory=true',
              options: {},
            },
          ],
        },
        {
          test: /\.(sa|sc|c)ss$/,
          include: [path.resolve(__dirname, 'src')],
          use: [
            {
              loader: 'css-hot-loader',
            },
            {
              loader: MiniCssExtractPlugin.loader,
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: !isProduction,
                url: false,
              },
            },
            {
              loader: 'postcss-loader',
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: !isProduction,
                outputStyle: isProduction ? 'compressed' : 'expanded',
              },
            },
          ],
        },
        {
          test: /\.hb$/,
          include: [path.resolve(__dirname, 'src')],
          use: [],
        },
      ],
    },

    mode: isProduction ? 'production' : 'development',

    entry: {
      'foe.campaignion': './src/campaignion.js',
    },

    output: {
      filename: isProduction ? 'js/[name].js' : 'js/[name].js',
      path: path.resolve(__dirname, '.'),
    },

    optimization: {
      splitChunks: {
        cacheGroups: {
          vendors: {
            priority: -10,
            test: /[\\/]node_modules[\\/]/,
          },
        },

        chunks: 'async',
        minChunks: 1,
        minSize: 30000,
        name: true,
      },
      minimizer: [
        new UglifyJSPlugin({
          // output: {
          //   comments: !isProduction,
          // },
          cache: isProduction,
          parallel: true,
          sourceMap: false,
        }),
      ],
    },

    performance: {
      hints: false,
    },

    plugins, // equals to plugins: plugins, ES6 magic.

    resolve: {
      modules: [
        'node_modules',
      ],
    },

  };
};
